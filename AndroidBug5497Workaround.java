package com.onevcat.uniwebview;

import android.app.Activity;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;

class AndroidBug5497Workaround {
   private FrameLayout mContent;
   private View mChildOfContent;
   private Activity mActivity;
   private int usableHeightPrevious;
   private int lastOrientation;
   private LayoutParams frameLayoutParams;
   private float mTargetHeight;

   static AndroidBug5497Workaround assistFrameLayout(FrameLayout frameLayout, Activity activity) {
      return new AndroidBug5497Workaround(frameLayout, activity);
   }

   private AndroidBug5497Workaround(FrameLayout frameLayout, Activity activity) {
      this.mActivity = activity;
      this.mContent = frameLayout;
      this.mChildOfContent = frameLayout.getChildAt(0);
      this.mChildOfContent.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
         public void onGlobalLayout() {
            AndroidBug5497Workaround.this.possiblyResizeChildOfContent();
         }
      });
      this.frameLayoutParams = (LayoutParams)frameLayout.getLayoutParams();
   }

   private void possiblyResizeChildOfContent() {
      int currentOrientation = this.mActivity.getResources().getConfiguration().orientation;
      int usableHeightNow = this.computeUsableHeight();
      if (currentOrientation != this.lastOrientation) {
         this.lastOrientation = currentOrientation;
         this.usableHeightPrevious = usableHeightNow;
      } else if (usableHeightNow != this.usableHeightPrevious) {
         this.frameLayoutParams.height = (int)(this.mTargetHeight - this.overlapHeight());
         this.mContent.requestLayout();
         this.usableHeightPrevious = usableHeightNow;
      }

   }

   private int computeUsableHeight() {
      Rect r = new Rect();
      this.mChildOfContent.getWindowVisibleDisplayFrame(r);
      return r.bottom - r.top;
   }

   private float contentMaxY() {
      return this.mTargetHeight + this.mChildOfContent.getY();
   }

   private float keyboardHeight() {
      return (float)(this.mChildOfContent.getRootView().getHeight() - this.computeUsableHeight());
   }

   private float screenHeight() {
      return (float)this.mChildOfContent.getRootView().getHeight();
   }

   private float overlapHeight() {
      return Math.max(0.0F, this.contentMaxY() + this.keyboardHeight() - this.screenHeight());
   }

   void setTargetHeight(float targetHeight) {
      this.mTargetHeight = targetHeight;
   }
}
