package com.onevcat.uniwebview;

import android.util.Log;

class Logger {
   static final int VERBOSE = 0;
   static final int DEBUG = 10;
   static final int INFO = 20;
   static final int CRITICAL = 80;
   static final int OFF = 99;
   private String tag;
   private int level;
   private static Logger instance = null;

   Logger(String tag, int level) {
      this.tag = tag;
      this.level = level;
   }

   static Logger getInstance() {
      if (instance == null) {
         instance = new Logger("UniWebView", 80);
      }

      return instance;
   }

   void verbose(String message) {
      this.log(0, message);
   }

   void debug(String message) {
      this.log(10, message);
   }

   void info(String message) {
      this.log(20, message);
   }

   void critical(String message) {
      this.log(80, message);
   }

   private void log(int level, String message) {
      if (level >= this.getLevel()) {
         if (level == 80) {
            Log.e(this.tag, "<UniWebView-Android> " + message);
         } else {
            Log.d(this.tag, "<UniWebView-Android> " + message);
         }
      }

   }

   public int getLevel() {
      return this.level;
   }

   public void setLevel(int level) {
      this.level = level;
      this.log(99, "Setting logging level to " + level);
   }
}
