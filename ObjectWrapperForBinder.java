package com.onevcat.uniwebview;

import android.os.Binder;

public class ObjectWrapperForBinder extends Binder {
   private final Object mData;

   public ObjectWrapperForBinder(Object data) {
      this.mData = data;
   }

   public Object getData() {
      return this.mData;
   }
}
