package com.onevcat.uniwebview;

interface OnKeyboardVisibilityListener {
   void onVisibilityChanged(boolean var1);
}
