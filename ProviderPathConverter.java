package com.onevcat.uniwebview;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore.Images.Media;
import android.text.TextUtils;

class ProviderPathConverter {
   static String getPath(Context context, Uri uri) {
      if (DocumentsContract.isDocumentUri(context, uri)) {
         String id;
         String[] split;
         String type;
         if (isExternalStorageDocument(uri)) {
            id = DocumentsContract.getDocumentId(uri);
            split = id.split(":");
            type = split[0];
            if ("primary".equalsIgnoreCase(type)) {
               return Environment.getExternalStorageDirectory() + "/" + split[1];
            }
         } else {
            if (isDownloadsDocument(uri)) {
               id = DocumentsContract.getDocumentId(uri);
               if (!TextUtils.isEmpty(id)) {
                  if (id.startsWith("raw:")) {
                     return id.replaceFirst("raw:", "");
                  }

                  try {
                     Uri contentUri = ContentUris.withAppendedId(Uri.parse(Environment.DIRECTORY_DOWNLOADS), Long.valueOf(id));
                     return getDataColumn(context, contentUri, (String)null, (String[])null);
                  } catch (NumberFormatException var8) {
                     return null;
                  }
               }

               return null;
            }

            if (isMediaDocument(uri)) {
               id = DocumentsContract.getDocumentId(uri);
               split = id.split(":");
               type = split[0];
               Uri contentUri = null;
               if ("image".equals(type)) {
                  contentUri = Media.EXTERNAL_CONTENT_URI;
               } else if ("video".equals(type)) {
                  contentUri = android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
               } else if ("audio".equals(type)) {
                  contentUri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
               }

               String selection = "_id=?";
               String[] selectionArgs = new String[]{split[1]};
               return getDataColumn(context, contentUri, "_id=?", selectionArgs);
            }
         }
      } else {
         if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, (String)null, (String[])null);
         }

         if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
         }
      }

      return null;
   }

   private static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
      Cursor cursor = null;
      String column = "_data";
      String[] projection = new String[]{"_data"};

      String var8;
      try {
         cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, (String)null);
         if (cursor == null || !cursor.moveToFirst()) {
            return null;
         }

         int column_index = cursor.getColumnIndexOrThrow("_data");
         var8 = cursor.getString(column_index);
      } finally {
         if (cursor != null) {
            cursor.close();
         }

      }

      return var8;
   }

   private static boolean isExternalStorageDocument(Uri uri) {
      return "com.android.externalstorage.documents".equals(uri.getAuthority());
   }

   private static boolean isDownloadsDocument(Uri uri) {
      return "com.android.providers.downloads.documents".equals(uri.getAuthority());
   }

   private static boolean isMediaDocument(Uri uri) {
      return "com.android.providers.media.documents".equals(uri.getAuthority());
   }
}
