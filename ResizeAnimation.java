package com.onevcat.uniwebview;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public class ResizeAnimation extends Animation {
   private int startWidth;
   private int deltaWidth;
   private int startHeight;
   private int deltaHeight;
   private int originalWidth;
   private int originalHeight;
   private View view;
   private boolean fillEnabled = false;

   public ResizeAnimation(View v, int startW, int endW, int startH, int endH) {
      this.view = v;
      this.startWidth = startW;
      this.deltaWidth = endW - startW;
      this.startHeight = startH;
      this.deltaHeight = endH - startH;
      this.originalHeight = v.getHeight();
      this.originalWidth = v.getWidth();
   }

   public void setFillEnabled(boolean enabled) {
      this.fillEnabled = enabled;
      super.setFillEnabled(enabled);
   }

   protected void applyTransformation(float interpolatedTime, Transformation t) {
      if ((double)interpolatedTime == 1.0D && !this.fillEnabled) {
         this.view.getLayoutParams().height = this.originalHeight;
         this.view.getLayoutParams().width = this.originalWidth;
      } else {
         if (this.deltaHeight != 0) {
            this.view.getLayoutParams().height = (int)((float)this.startHeight + (float)this.deltaHeight * interpolatedTime);
         }

         if (this.deltaWidth != 0) {
            this.view.getLayoutParams().width = (int)((float)this.startWidth + (float)this.deltaWidth * interpolatedTime);
         }
      }

      this.view.requestLayout();
   }
}
