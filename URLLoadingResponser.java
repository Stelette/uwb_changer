package com.onevcat.uniwebview;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

class URLLoadingResponser {
   private final Context context;
   private final UniWebViewDialog dialog;
   private final Logger logger;
   private String url;

   private String getLowerUrl() {
      return this.url.toLowerCase(Locale.ROOT);
   }

   URLLoadingResponser(Context context, UniWebViewDialog dialog, String url) {
      this.context = context;
      this.dialog = dialog;
      this.url = url;
      this.logger = Logger.getInstance();
   }

   Intent smsIntent() {
      if (!this.getLowerUrl().startsWith("sms:")) {
         return null;
      } else {
         this.logger.debug("Received sms url...");

         try {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(this.url));
            intent.addCategory("android.intent.category.DEFAULT");
            intent.addCategory("android.intent.category.BROWSABLE");
            return intent;
         } catch (Exception var2) {
            this.logger.critical("sms url intent open exception: " + var2.getMessage());
            return null;
         }
      }
   }

   private Intent telIntent() {
      if (!this.getLowerUrl().startsWith("tel:")) {
         return null;
      } else {
         this.logger.debug("Received tel url...");
         return new Intent("android.intent.action.DIAL", Uri.parse(this.url));
      }
   }

   private Intent mailToIntent() {
      if (!this.getLowerUrl().startsWith("mailto:")) {
         return null;
      } else {
         this.logger.debug("Received mailto url...");
         return new Intent("android.intent.action.SENDTO", Uri.parse(this.url));
      }
   }

   private Intent explicitlyIntent() {
      if (!this.getLowerUrl().startsWith("intent:")) {
         return null;
      } else {
         this.logger.debug("Received intent url...");

         try {
            Intent intent = Intent.parseUri(this.url, 1);
            ResolveInfo info = this.context.getPackageManager().resolveActivity(intent, 65536);
            if (info != null) {
               return intent;
            } else {
               String intentId = intent.getPackage();
               if (intentId == null) {
                  return null;
               } else {
                  Intent marketIntent = new Intent("android.intent.action.VIEW");
                  marketIntent.setData(Uri.parse("market://details?id=" + intentId));
                  return marketIntent;
               }
            }
         } catch (Exception var5) {
            this.logger.critical("Parsing intent url error. " + var5.getMessage());
            return null;
         }
      }
   }

   private Intent marketIntent() {
      if (!this.getLowerUrl().startsWith("market:")) {
         return null;
      } else {
         this.logger.debug("Received market url...");

         try {
            return Intent.parseUri(this.url, 1);
         } catch (URISyntaxException var2) {
            this.logger.critical("Parsing market url error. " + var2.getMessage());
            return null;
         }
      }
   }

   private Intent thirdPartyAppIntent() {
      String loweredUrl = this.getLowerUrl();
      if (!loweredUrl.startsWith("http:") && !loweredUrl.startsWith("https:") && !loweredUrl.startsWith("file:") && !loweredUrl.startsWith("about:blank")) {
         Uri uri = Uri.parse(this.url);
         Intent intent = new Intent("android.intent.action.VIEW", uri);
         PackageManager packageManager = this.context.getPackageManager();
         List<ResolveInfo> activities = packageManager.queryIntentActivities(intent, 0);
         return activities.size() == 0 ? null : intent;
      } else {
         return null;
      }
   }

   private boolean tryToRunIntent(Intent intent) {
      if (intent != null) {
         if (this.context != null) {
            try {
               this.context.startActivity(intent);
            } catch (Exception var3) {
               this.logger.critical("No Activity found to handle Intent: " + intent.getData());
            }
         }

         return true;
      } else {
         return false;
      }
   }

   boolean handleWithIntent() {
      this.logger.verbose("Checking url could be handled with any intents: " + this.url);
      return this.tryToRunIntent(this.mailToIntent()) || this.tryToRunIntent(this.telIntent()) || this.tryToRunIntent(this.smsIntent()) || this.tryToRunIntent(this.explicitlyIntent()) || this.tryToRunIntent(this.marketIntent());
   }

   boolean handleWithThirdPartyApp() {
      this.logger.verbose("Checking url could be handled with any third party apps: " + this.url);
      return this.tryToRunIntent(this.thirdPartyAppIntent());
   }

   boolean canResponseDefinedScheme() {
      this.logger.verbose("Checking url could match with a defined url scheme: " + this.url);
      Iterator var1 = this.dialog.getSchemes().iterator();

      String scheme;
      do {
         if (!var1.hasNext()) {
            this.logger.verbose("Did not find a matched scheme for: " + this.url);
            return false;
         }

         scheme = (String)var1.next();
      } while(!this.url.startsWith(scheme + "://"));

      this.logger.verbose("Found url match scheme: " + this.url);
      return true;
   }

   boolean canResponseBuiltinScheme() {
      if (this.url.startsWith("uniwebviewinternal://")) {
         if (this.url.contains("__uniwebview_internal_video_end")) {
            this.dialog.getWebView().notifyVideoEnd();
         }

         return true;
      } else {
         return false;
      }
   }
}
