package com.onevcat.uniwebview;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Environment;
import android.os.Build.VERSION;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.webkit.CookieManager;
import android.webkit.MimeTypeMap;
import android.webkit.URLUtil;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView.HitTestResult;
import android.widget.Toast;
import android.widget.FrameLayout.LayoutParams;
import com.onevcat.uniwebview.R.string;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map.Entry;

class UniWebView extends VideoEnabledWebView {
   private static boolean allowAutoPlay = false;
   private static boolean allowJavaScriptOpenWindow = false;
   private static boolean javaScriptEnabled = true;
   private UniWebViewClient client;
   private Activity activity;
   boolean calloutEnabled = true;

   static void setAllowAutoPlay(boolean flag) {
      allowAutoPlay = flag;
   }

   static void setAllowJavaScriptOpenWindow(boolean flag) {
      allowJavaScriptOpenWindow = flag;
   }

   static void setJavaScriptEnabled(boolean flag) {
      javaScriptEnabled = flag;
   }

   @SuppressLint({"SetJavaScriptEnabled"})
   UniWebView(Context context) {
      super(context);
      this.activity = (Activity)context;
      WebSettings webSettings = this.getSettings();
      webSettings.setJavaScriptEnabled(true);
      webSettings.setDatabaseEnabled(true);
      webSettings.setDomStorageEnabled(true);
      webSettings.setAllowFileAccess(true);
      webSettings.setGeolocationEnabled(true);
      webSettings.setDisplayZoomControls(false);
      webSettings.setAllowFileAccessFromFileURLs(true);
      webSettings.setAllowUniversalAccessFromFileURLs(true);
      webSettings.setMediaPlaybackRequiresUserGesture(!allowAutoPlay);
      webSettings.setJavaScriptCanOpenWindowsAutomatically(allowJavaScriptOpenWindow);
      webSettings.setJavaScriptEnabled(javaScriptEnabled);
      webSettings.setMixedContentMode(2);
      String cachePath = context.getCacheDir().getPath();
      webSettings.setAppCachePath(cachePath);
      webSettings.setAppCacheEnabled(true);
      this.setScrollBarStyle(0);
      LayoutParams params = new LayoutParams(-1, -1);
      this.setLayoutParams(params);
   }

   static void clearCookies() {
      CookieManager cm = CookieManager.getInstance();
      cm.removeAllCookies((ValueCallback)null);
      cm.flush();
      Logger.getInstance().verbose("Cookie manager flushed.");
   }

   static void setCookie(String url, String cookie) {
      Logger logger = Logger.getInstance();
      CookieManager cm = CookieManager.getInstance();
      logger.verbose("Cookie set for url: " + url + ". Content: " + cookie);
      cm.setCookie(url, cookie);
      cm.flush();
      logger.verbose("Cookie manager flushed.");
   }

   static String getCookie(String url, String key) {
      Logger logger = Logger.getInstance();
      String value = "";
      CookieManager cm = CookieManager.getInstance();
      String cookies = cm.getCookie(url);
      if (cookies == null) {
         Logger.getInstance().debug("The content for url is not found in cookie. Url: " + url);
         return "";
      } else {
         logger.verbose("Cookie string is found: " + cookies + ", for url: " + url);
         logger.verbose("Trying to parse cookie to find for key: " + key);
         String[] temp = cookies.split(";");
         String[] var7 = temp;
         int var8 = temp.length;

         for(int var9 = 0; var9 < var8; ++var9) {
            String kvPair = var7[var9];
            if (kvPair.contains(key)) {
               String[] pair = kvPair.split("=", 2);
               if (pair.length >= 2) {
                  value = pair[1];
                  logger.verbose("Found cookie value: " + value + ", for key: " + key);
               }
            }
         }

         return value;
      }
   }

   public UniWebViewClient getClient() {
      return this.client;
   }

   public void setClient(UniWebViewClient client) {
      this.client = client;
      this.setWebViewClient(client);
   }

   public HashMap<String, String> getCustomizeHeaders() {
      throw new RuntimeException("Stub!");
   }

   protected void onCreateContextMenu(ContextMenu menu) {
      if (this.calloutEnabled) {
         super.onCreateContextMenu(menu);
         HitTestResult webViewHitTestResult = this.getHitTestResult();
         int hitType = webViewHitTestResult.getType();
         if (hitType == 5 || hitType == 8) {
            final String downloadUrl = webViewHitTestResult.getExtra();
            if (!downloadUrl.toLowerCase(Locale.ROOT).startsWith("http://") && !downloadUrl.toLowerCase(Locale.ROOT).startsWith("https://")) {
               return;
            }

            menu.setHeaderTitle(downloadUrl);
            final Resources res = this.getContext().getResources();
            String buttonTitle = res.getString(string.SAVE_IMAGE);
            menu.add(0, 1, 0, buttonTitle).setOnMenuItemClickListener(new OnMenuItemClickListener() {
               public boolean onMenuItemClick(MenuItem menuItem) {
                  String fileName;
                  if (VERSION.SDK_INT >= 23 && UniWebView.this.activity.checkSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE") != 0) {
                     UniWebView.this.activity.requestPermissions(new String[]{"android.permission.WRITE_EXTERNAL_STORAGE"}, 1);
                     fileName = res.getString(string.EXTERNAL_WRITE_PERMISSION_LACK);
                     Toast.makeText(UniWebView.this.getContext(), fileName, 1).show();
                     return false;
                  } else {
                     if (URLUtil.isValidUrl(downloadUrl)) {
                        fileName = URLUtil.guessFileName(downloadUrl, (String)null, MimeTypeMap.getFileExtensionFromUrl(downloadUrl));
                        Request request = new Request(Uri.parse(downloadUrl));
                        request.allowScanningByMediaScanner();
                        request.setNotificationVisibility(1);
                        request.setDescription(fileName);
                        request.setTitle(fileName);
                        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
                        HashMap<String, String> headers = UniWebView.this.getCustomizeHeaders();
                        Iterator var5 = headers.entrySet().iterator();

                        while(var5.hasNext()) {
                           Entry<String, String> entry = (Entry)var5.next();
                           String key = (String)entry.getKey();
                           String value = (String)entry.getValue();
                           request.addRequestHeader(key, value);
                        }

                        DownloadManager downloadManager = (DownloadManager)UniWebView.this.getContext().getSystemService("download");
                        downloadManager.enqueue(request);
                        String downloadStartedText = res.getString(string.DOWNLOAD_STARTED);
                        Toast.makeText(UniWebView.this.getContext(), downloadStartedText, 1).show();
                     } else {
                        fileName = res.getString(string.INVALID_URL);
                        Toast.makeText(UniWebView.this.getContext(), fileName, 1).show();
                     }

                     return false;
                  }
               }
            });
         }

      }
   }
}
