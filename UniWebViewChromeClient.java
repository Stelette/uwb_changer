package com.onevcat.uniwebview;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ClipData.Item;
import android.content.DialogInterface.OnClickListener;
import android.database.Cursor;
import android.net.Uri;
import android.os.CancellationSignal;
import android.os.Environment;
import android.os.Message;
import android.text.Editable;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.PermissionRequest;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.GeolocationPermissions.Callback;
import android.webkit.WebChromeClient.FileChooserParams;
import android.webkit.WebView.WebViewTransport;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class UniWebViewChromeClient extends VideoEnabledWebChromeClient {
   private Activity activity;
   private UniWebViewDialog dialog;
   private AlertDialog alertDialog;
   private ValueCallback<Uri[]> callback;
   private String cameraPhotoPath;
   private FileChooserParams params;
   private UniWebView popUpWebView;

   AlertDialog getAlertDialog() {
      return this.alertDialog;
   }

   UniWebViewChromeClient(Activity activity, View activityNonVideoView, ViewGroup activityVideoView, View loadingView, UniWebView webView, UniWebViewDialog dialog) {
      super(activityNonVideoView, activityVideoView, loadingView, webView);
      this.activity = activity;
      this.dialog = dialog;
   }

   private UniWebViewChromeClient(Activity activity, UniWebViewDialog dialog) {
      this.activity = activity;
      this.dialog = dialog;
   }

   public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
      Logger.getInstance().info("UniWebViewChromeClient onShowFileChooser.");
      if (this.callback != null) {
         Logger.getInstance().critical("Trying to show another file chooser before previous one finished. Discard previous upload!");
         this.callback.onReceiveValue((Object)null);
      }

      this.callback = filePathCallback;
      this.params = fileChooserParams;
      Logger.getInstance().verbose("Start file chooser activity.");
      Intent intent = new Intent(this.activity, UniWebViewFileChooserActivity.class);
      intent.putExtra("chrome_client", this.dialog.getWebViewName());
      this.activity.startActivity(intent);
      return true;
   }

   public void onPermissionRequest(final PermissionRequest request) {
      Logger.getInstance().info("UniWebViewChromeClient onPermissionRequest, url: " + request.getOrigin().toString());
      this.activity.runOnUiThread(new Runnable() {
         public void run() {
            try {
               URL url = new URL(request.getOrigin().toString());
               if (UniWebViewChromeClient.this.dialog.getPermissionTrustDomains().contains(url.getHost())) {
                  Logger.getInstance().info("Permission domain '" + url.getHost() + "' contains in permission trusted domains. Granting...");
                  request.grant(request.getResources());
               } else {
                  Logger.getInstance().critical("Permission domain '" + url.getHost() + "' is not allowed. Deny request.");
                  Logger.getInstance().critical("If you want to allow permission access from this domain, add it through `UniWebView.AddPermissionTrustDomain` first");
                  request.deny();
               }
            } catch (MalformedURLException var2) {
               Logger.getInstance().critical("onPermissionRequest failed due to malformed url exception. " + var2.getMessage());
               request.deny();
            }

         }
      });
   }

   FileChooserParams getFileChooserParams() {
      return this.params;
   }

   private void showAlert() {
      if (this.dialog.getImmersiveMode()) {
         this.alertDialog.getWindow().setFlags(8, 8);
         this.alertDialog.show();
         this.alertDialog.getWindow().getDecorView().setSystemUiVisibility(this.dialog.getWindow().getDecorView().getSystemUiVisibility());
         this.alertDialog.getWindow().clearFlags(8);
      } else {
         this.alertDialog.show();
      }

   }

   public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
      Builder alertDialogBuilder = new Builder(this.dialog.getContext());
      this.alertDialog = alertDialogBuilder.setTitle(url).setMessage(message).setCancelable(false).setIcon(17301543).setPositiveButton(17039370, new OnClickListener() {
         public void onClick(DialogInterface dialog, int id) {
            dialog.dismiss();
            result.confirm();
            UniWebViewChromeClient.this.alertDialog = null;
         }
      }).create();
      this.showAlert();
      return true;
   }

   public boolean onJsConfirm(WebView view, String url, String message, final JsResult result) {
      Builder alertDialogBuilder = new Builder(this.dialog.getContext());
      this.alertDialog = alertDialogBuilder.setTitle(url).setMessage(message).setIcon(17301659).setCancelable(false).setPositiveButton(17039379, new OnClickListener() {
         public void onClick(DialogInterface dialog, int whichButton) {
            dialog.dismiss();
            result.confirm();
            UniWebViewChromeClient.this.alertDialog = null;
         }
      }).setNegativeButton(17039369, new OnClickListener() {
         public void onClick(DialogInterface dialog, int i) {
            dialog.dismiss();
            result.cancel();
            UniWebViewChromeClient.this.alertDialog = null;
         }
      }).create();
      this.showAlert();
      return true;
   }

   public boolean onJsPrompt(WebView view, String url, String message, String defaultValue, final JsPromptResult result) {
      Builder alertDialogBuilder = new Builder(this.dialog.getContext());
      alertDialogBuilder.setTitle(url).setMessage(message).setIcon(17301659).setCancelable(false);
      final EditText input = new EditText(this.dialog.getContext());
      input.setSingleLine();
      alertDialogBuilder.setView(input);
      alertDialogBuilder.setPositiveButton(17039379, new OnClickListener() {
         public void onClick(DialogInterface dialog, int whichButton) {
            Editable editable = input.getText();
            String value = "";
            if (editable != null) {
               value = editable.toString();
            }

            dialog.dismiss();
            result.confirm(value);
            UniWebViewChromeClient.this.alertDialog = null;
         }
      });
      alertDialogBuilder.setNegativeButton(17039369, new OnClickListener() {
         public void onClick(DialogInterface dialog, int whichButton) {
            dialog.dismiss();
            result.cancel();
            UniWebViewChromeClient.this.alertDialog = null;
         }
      });
      this.alertDialog = alertDialogBuilder.create();
      this.showAlert();
      return true;
   }

   public void onGeolocationPermissionsShowPrompt(String origin, Callback callback) {
      callback.invoke(origin, true, false);
   }

   @SuppressLint({"SetJavaScriptEnabled"})
   public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
      Logger.getInstance().info("onCreateWindow: " + view + ", isUserGesture: " + isUserGesture);
      if (this.dialog.getWebView().getSettings().supportMultipleWindows() && isUserGesture) {
         UniWebView newWebView = new UniWebView(this.activity);
         newWebView.getSettings().setJavaScriptEnabled(true);
         newWebView.setWebChromeClient(new UniWebViewChromeClient(this.activity, this.dialog));
         newWebView.setWebViewClient(new WebViewClient());
         LayoutParams params = new LayoutParams(-1, -1);
         newWebView.setLayoutParams(params);
         newWebView.getSettings().setUserAgentString(this.dialog.getWebView().getSettings().getUserAgentString());
         view.addView(newWebView);
         WebViewTransport transport = (WebViewTransport)resultMsg.obj;
         transport.setWebView(newWebView);
         resultMsg.sendToTarget();
         this.setPopUpWebView(newWebView);
         return true;
      } else {
         return super.onCreateWindow(view, isDialog, isUserGesture, resultMsg);
      }
   }

   public void onCloseWindow(WebView window) {
      super.onCloseWindow(window);
      this.dialog.getWebView().removeView(window);
      this.setPopUpWebView((UniWebView)null);
   }

   void receivedFileValue(Intent intent, boolean hasImage) {
      Uri[] results = null;
      if (intent != null) {
         String uri = intent.getDataString();
         ArrayList<Uri> uris = new ArrayList();
         int i;
         if (uri != null) {
            uris.add(Uri.parse(uri));
         } else {
            ClipData mClipData = intent.getClipData();
            if (mClipData != null) {
               for(i = 0; i < mClipData.getItemCount(); ++i) {
                  Item item = mClipData.getItemAt(i);
                  Uri clipUri = item.getUri();
                  uris.add(clipUri);
               }
            }
         }

         if (uris.isEmpty()) {
            if (hasImage && this.cameraPhotoPath != null) {
               results = new Uri[]{Uri.parse(this.cameraPhotoPath)};
            }
         } else {
            ArrayList<Uri> convertedResults = new ArrayList();

            for(i = 0; i < uris.size(); ++i) {
               Uri contentUri = (Uri)uris.get(i);
               String path = this.uriToFilename(contentUri);
               if (path != null) {
                  convertedResults.add(Uri.fromFile(new File(path)));
               } else {
                  try {
                     InputStream input = this.activity.getContentResolver().openInputStream(contentUri);
                     File f = this.createTempFile(contentUri);
                     this.copyInputStreamToFile(input, f);
                     convertedResults.add(Uri.fromFile(f));
                  } catch (FileNotFoundException var12) {
                     var12.printStackTrace();
                     Logger.getInstance().critical("Can not get correct path on disk storage.");
                  } catch (IOException var13) {
                     var13.printStackTrace();
                     Logger.getInstance().critical("Can not get correct path on disk storage.");
                  }
               }
            }

            results = (Uri[])convertedResults.toArray(new Uri[0]);
         }
      } else if (hasImage && this.cameraPhotoPath != null) {
         results = new Uri[]{Uri.parse(this.cameraPhotoPath)};
      }

      if (this.callback != null) {
         this.callback.onReceiveValue(results);
      }

      this.callback = null;
      this.params = null;
   }

   private void copyInputStreamToFile(InputStream in, File file) {
      try {
         OutputStream out = new FileOutputStream(file);
         byte[] buf = new byte[1024];

         int len;
         while((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
         }

         out.close();
         in.close();
      } catch (Exception var6) {
         var6.printStackTrace();
      }

   }

   private String uriToFilename(Uri uri) {
      return ProviderPathConverter.getPath(this.activity, uri);
   }

   File createImageFile() throws IOException {
      String timeStamp = (new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US)).format(new Date());
      String imageFileName = "IMAGE_" + timeStamp + "_";
      File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
      if (!storageDir.exists()) {
         storageDir.mkdir();
      }

      return File.createTempFile(imageFileName, ".png", storageDir);
   }

   File createTempFile(Uri uri) throws IOException {
      Cursor cursor = this.activity.getContentResolver().query(uri, (String[])null, (String)null, (String[])null, (String)null, (CancellationSignal)null);
      String displayName = "";
      if (cursor != null && cursor.moveToFirst()) {
         displayName = cursor.getString(cursor.getColumnIndex("_display_name"));
      }

      cursor.close();
      return File.createTempFile(displayName, (String)null, (File)null);
   }

   void setCameraPhotoPath(String cameraPhotoPath) {
      this.cameraPhotoPath = cameraPhotoPath;
   }

   String getCameraPhotoPath() {
      return this.cameraPhotoPath;
   }

   public UniWebView getPopUpWebView() {
      return this.popUpWebView;
   }

   public void setPopUpWebView(UniWebView popUpWebView) {
      this.popUpWebView = popUpWebView;
   }
}
