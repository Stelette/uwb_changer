package com.onevcat.uniwebview;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.net.http.SslCertificate;
import android.net.http.SslError;
import android.text.method.PasswordTransformationMethod;
import android.webkit.HttpAuthHandler;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.onevcat.uniwebview.R.string;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

class UniWebViewClient extends WebViewClient {
   private UniWebViewDialog dialog;
   private boolean loadingSuccess;
   private boolean userStopped;
   private boolean sslErrored;
   private int httpStatusCode = 200;

   UniWebViewClient(UniWebViewDialog dialog) {
      this.dialog = dialog;
   }

   public void onPageStarted(WebView view, String url, Bitmap favicon) {
      super.onPageStarted(view, url, favicon);
      Logger.getInstance().info("WebClient onPageStarted: " + url);
      this.dialog.setLoading(true);
      if (this.dialog.isShowSpinnerWhileLoading() && this.dialog.isShowing()) {
         this.dialog.showSpinner();
      }

      this.dialog.hideSystemUI();
      this.dialog.listener.onPageStarted(this.dialog, url);
   }

   public void onPageFinished(WebView view, String url) {
      super.onPageFinished(view, url);
      Logger.getInstance().info("WebClient onPageFinished: " + url);
      this.dialog.setLoading(false);
      this.dialog.hideSpinner();
      this.dialog.hideSystemUI();
      if (this.loadingSuccess) {
         if (this.sslErrored) {
            Logger.getInstance().critical("WebClient onReceivedError for url: " + url + " Error Code: " + -1202 + " Error: ssl error");
            this.dialog.listener.onReceivedError(this.dialog, -1202, "ssl error", url);
         } else if (this.userStopped) {
            Logger.getInstance().critical("WebClient onReceivedError for url: " + url + " Error Code: " + 999 + " Error: Operation cancelled.");
            this.dialog.listener.onReceivedError(this.dialog, -999, "Operation cancelled.", url);
         } else {
            Logger.getInstance().info("WebClient onPageFinished: " + url + ". Status Code:" + this.httpStatusCode + " Loading success: " + this.loadingSuccess);
            this.dialog.listener.onPageFinished(this.dialog, this.httpStatusCode, url);
         }
      }

   }

   public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
      Logger.getInstance().info("WebClient onReceivedHttpError. Code: " + errorResponse.getStatusCode());
      if (request.isForMainFrame()) {
         this.httpStatusCode = errorResponse.getStatusCode();
      }

   }

   public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
      super.onReceivedError(view, errorCode, description, failingUrl);
      Logger.getInstance().critical("WebClient onReceivedError for url: " + failingUrl + " Error Code: " + errorCode + " Error: " + description);
      this.loadingSuccess = false;
      this.dialog.hideSpinner();
      this.dialog.hideSystemUI();
      this.dialog.listener.onReceivedError(this.dialog, errorCode, description, failingUrl);
   }

   public boolean shouldOverrideUrlLoading(WebView view, String url) {
      Logger.getInstance().info("WebClient shouldOverrideUrlLoading: " + url);
      if (!this.dialog.listener.shouldOverrideUrlLoading(this.dialog, url)) {
         if (url.startsWith("file://")) {
            Logger.getInstance().debug("Loading a local file. The local file loading will never be overridden.");
            return false;
         } else {
            HashMap<String, String> headers = this.dialog.getHeaders();
            if (!headers.isEmpty()) {
               Logger.getInstance().debug("Adding customized header to request. " + headers.toString());
               view.loadUrl(url, headers);
               return true;
            } else {
               return false;
            }
         }
      } else {
         return true;
      }
   }

   public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
      Logger.getInstance().critical("WebClient onReceivedSslError. Error url: " + error.getUrl() + " Error type: " + error.getPrimaryError());

      try {
         Logger.getInstance().verbose("Trying to process SSL error...");
         URL url = new URL(error.getUrl());
         String host = url.getHost();
         SslCertificate certificate = error.getCertificate();
         if (certificate.getIssuedBy() == null || certificate.getIssuedTo() == null) {
            Logger.getInstance().verbose("Cannot get correct certificate issuer. SSL challenge failed.");
            handler.cancel();
            return;
         }

         if (this.dialog.getSslExceptionDomains().contains(host)) {
            Logger.getInstance().verbose("Found domain '" + host + "' in sslExceptionDomains, proceeding url...");
            handler.proceed();
         } else {
            Logger.getInstance().verbose("Domain '" + host + "' is not in exception. Refuse proceeding url.");
            this.sslErrored = true;
            handler.cancel();
         }
      } catch (MalformedURLException var7) {
         Logger.getInstance().verbose("Url '" + error.getUrl() + "' is malformed. Refuse proceeding url. Exception: " + var7);
         this.sslErrored = true;
         handler.cancel();
      }

   }

   public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {
      String username = null;
      String password = null;
      if (handler.useHttpAuthUsernamePassword() && view != null) {
         String[] haup = view.getHttpAuthUsernamePassword(host, realm);
         if (haup != null && haup.length == 2) {
            username = haup[0];
            password = haup[1];
         }
      }

      if (username != null && password != null) {
         handler.proceed(username, password);
      } else if (this.dialog.getShowHTTPAuthPopUpWindow()) {
         this.showHttpAuthDialog(view, handler, host, realm);
      } else {
         handler.cancel();
      }

   }

   private void showHttpAuthDialog(final WebView view, final HttpAuthHandler handler, final String host, final String realm) {
      Context context = this.dialog.getContext();
      LinearLayout layout = new LinearLayout(context);
      final EditText userText = new EditText(context);
      userText.setHint(context.getResources().getString(string.USERNAME));
      final EditText passwordText = new EditText(context);
      passwordText.setHint(context.getResources().getString(string.PASSWORD));
      passwordText.setTransformationMethod(PasswordTransformationMethod.getInstance());
      layout.setOrientation(1);
      layout.addView(userText);
      layout.addView(passwordText);
      Builder mHttpAuthDialog = new Builder(context);
      String authTitleText = context.getResources().getString(string.AUTH_REQUIRE_TITLE);
      mHttpAuthDialog.setTitle(authTitleText).setMessage(host).setView(layout).setCancelable(false).setPositiveButton(context.getString(17039370), new OnClickListener() {
         public void onClick(DialogInterface dialog, int whichButton) {
            String username = userText.getText().toString();
            String password = passwordText.getText().toString();
            view.setHttpAuthUsernamePassword(host, realm, username, password);
            handler.proceed(username, password);
         }
      }).setNegativeButton(context.getString(17039360), new OnClickListener() {
         public void onClick(DialogInterface dialog, int whichButton) {
            handler.cancel();
         }
      }).create().show();
   }

   public void setUserStopped(boolean userStopped) {
      this.userStopped = userStopped;
   }

   public void prepareLoadingStates() {
      this.httpStatusCode = 200;
      this.loadingSuccess = true;
      this.userStopped = false;
      this.sslErrored = false;
   }
}
