package com.onevcat.uniwebview;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Build.VERSION;
import android.print.PrintDocumentAdapter;
import android.print.PrintManager;
import android.print.PrintAttributes.Builder;
import android.util.TypedValue;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnTouchListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.WindowManager.LayoutParams;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.widget.FrameLayout;
import com.onevcat.uniwebview.R.string;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class UniWebViewDialog extends Dialog implements OnKeyboardVisibilityListener {
   final UniWebViewDialog.DialogListener listener;
   private HashSet<String> schemes;
   private HashMap<String, String> headers;
   private HashSet<String> permissionTrustDomains;
   private HashSet<String> sslExceptionDomains;
   private UniWebView uniWebView;
   private boolean openLinksInExternalBrowser;
   private boolean immersiveMode = true;
   private boolean showHTTPAuthPopUpWindow = true;
   private boolean showSpinnerWhileLoading;
   private boolean isLoading;
   private Activity activity;
   private ProgressDialog spinner;
   private String spinnerText = null;
   private boolean userInteractionEnabled = true;
   private AndroidBug5497Workaround workaround;
   static final int ANIMATION_EDGE_NONE = 0;
   static final int ANIMATION_EDGE_TOP = 1;
   static final int ANIMATION_EDGE_LEFT = 2;
   static final int ANIMATION_EDGE_BOTTOM = 3;
   static final int ANIMATION_EDGE_RIGHT = 4;
   private int x;
   private int y;
   private int width;
   private int height;
   private boolean loadingInterrupted;
   private UniWebViewChromeClient chromeClient;
   private String webViewName;
   private boolean animating;
   private boolean backButtonEnabled = true;
   private float webViewAlpha = 1.0F;
   private boolean webViewShowing = false;
   private boolean webViewVisible = false;
   FrameLayout videoContainer;
   FrameLayout webViewContainer;
   static HashMap<String, String> presetUserAgent = new HashMap();
   static String defaultUserAgent = "";
   private String userAgent;
   boolean touchFromAnotherDialog;

   public UniWebViewChromeClient getChromeClient() {
      return this.chromeClient;
   }

   public String getWebViewName() {
      return this.webViewName;
   }

   public void setWebViewName(String webViewName) {
      this.webViewName = webViewName;
   }

   static String getUserAgent(UniWebViewDialog dialog, String name) {
      if (dialog != null) {
         return dialog.getUserAgent();
      } else {
         String value = (String)presetUserAgent.get(name);
         return value != null ? value : defaultUserAgent;
      }
   }

   static void setUserAgent(UniWebViewDialog dialog, String name, String ua) {
      if (dialog != null) {
         dialog.setUserAgent(ua);
         presetUserAgent.remove(name);
      } else {
         presetUserAgent.put(name, ua);
      }

   }

   private String getUserAgent() {
      return this.userAgent;
   }

   private void setUserAgent(String userAgent) {
      if (Looper.myLooper() == Looper.getMainLooper()) {
         this.getWebView().getSettings().setUserAgentString(userAgent);
      }

      this.userAgent = userAgent;
   }

   String getUrl() {
      return this.getWebView().getUrl();
   }

   boolean isCanGoBack() {
      return this.getWebView().canGoBack();
   }

   boolean isCanGoForward() {
      return this.getWebView().canGoForward();
   }

   @SuppressLint({"ClickableViewAccessibility"})
   UniWebViewDialog(Activity activity, UniWebViewDialog.DialogListener listener) {
      super(activity, 16973932);
      this.userAgent = defaultUserAgent;
      this.touchFromAnotherDialog = false;
      Logger.getInstance().debug("Creating new UniWebView dialog.");
      this.activity = activity;
      this.listener = listener;
      this.schemes = new HashSet();
      this.schemes.add("uniwebview");
      this.permissionTrustDomains = new HashSet();
      this.sslExceptionDomains = new HashSet();
      this.headers = new HashMap();
      this.prepareWindow();
      this.hideSystemUI();
      this.addWebViewContent();
      this.setBouncesEnabled(false);
      this.setKeyboardVisibilityListener(this);
      this.workaround = AndroidBug5497Workaround.assistFrameLayout(this.webViewContainer, activity);
      this.uniWebView.setOnTouchListener(new OnTouchListener() {
         public boolean onTouch(View v, MotionEvent event) {
            return !UniWebViewDialog.this.isUserInteractionEnabled();
         }
      });
   }

   public boolean onTouchEvent(MotionEvent event) {
      if (!this.touchFromAnotherDialog) {
         return UniWebViewManager.getInstance().handleTouchEvent(this, this.activity, event);
      } else {
         boolean visible = this.webViewContainer.getVisibility() == 0;
         return visible && this.isEventInside(event);
      }
   }

   private boolean isEventInside(MotionEvent event) {
      return this.isViewContains(this.uniWebView, (int)event.getRawX(), (int)event.getRawY());
   }

   private boolean isViewContains(View view, int rx, int ry) {
      int[] location = new int[2];
      view.getLocationOnScreen(location);
      int x = location[0];
      int y = location[1];
      int w = view.getWidth();
      int h = view.getHeight();
      return rx >= x && rx <= x + w && ry >= y && ry <= y + h;
   }

   public boolean onKeyDown(int keyCode, KeyEvent event) {
      Logger.getInstance().verbose("onKeyDown, key code: " + keyCode);
      if (keyCode != 4) {
         Logger.getInstance().verbose("Not back key. Delegating to super...");
         return super.onKeyDown(keyCode, event);
      } else if (!this.backButtonEnabled) {
         Logger.getInstance().verbose("Back button is not enabled. Ignore.");
         return true;
      } else {
         if (!this.goBack()) {
            Logger.getInstance().verbose("No back page for the web view. Trying to close current web view...");
            this.listener.onDialogClosedByBackButton(this);
         }

         return true;
      }
   }

   public boolean dispatchKeyEvent(KeyEvent event) {
      Logger.getInstance().verbose("dispatchKeyEvent: " + event);
      if (event.getAction() == 0) {
         int keyCode = event.getKeyCode();
         Logger.getInstance().verbose("Key down event for: " + keyCode);
         this.listener.onDialogKeyDown(this, keyCode);
      }

      return super.dispatchKeyEvent(event);
   }

   protected void onStop() {
      super.onStop();
      this.listener.onDialogClose(this);
   }

   boolean getAnimating() {
      return this.animating;
   }

   HashSet<String> getSchemes() {
      return this.schemes;
   }

   void setFrame(int x, int y, int width, int height) {
      Logger.getInstance().verbose(String.format(Locale.US, "Setting web dialog frame to {%d, %d, %d, %d}", x, y, width, height));
      this.setPosition(x, y);
      this.setSize(width, height);
   }

   void setPosition(int x, int y) {
      this.x = x;
      this.y = y;
      Window window = this.getWindow();
      LayoutParams params = window.getAttributes();
      params.gravity = 51;
      params.x = 0;
      params.y = 0;
      window.setAttributes(params);
      this.webViewContainer.setX((float)x);
      this.webViewContainer.setY((float)y);
   }

   void setSize(int width, int height) {
      this.width = Math.max(0, width);
      this.height = Math.max(0, height);
      Window window = this.getWindow();
      Point size = this.displaySize();
      window.setLayout(size.x, size.y);
      android.view.ViewGroup.LayoutParams p = this.webViewContainer.getLayoutParams();
      p.width = this.width;
      p.height = this.height;
      this.webViewContainer.setLayoutParams(p);
      this.workaround.setTargetHeight((float)this.height);
   }

   void updateFrame() {
      this.setPosition(this.x, this.y);
      this.setSize(this.width, this.height);
   }

   boolean setShow(final boolean show, boolean fade, int edge, float duration, final String identifier) {
      if (this.webViewVisible && show) {
         Logger.getInstance().critical("Showing web view is ignored since it is already visible.");
         return false;
      } else if (!this.webViewVisible && !show) {
         Logger.getInstance().critical("Hiding web view is ignored since it is already invisible.");
         return false;
      } else if (this.animating) {
         Logger.getInstance().critical("Trying to animate but another transition animation is not finished yet. Ignore this one.");
         return false;
      } else {
         if (this.webViewShowing) {
            boolean alreadyVisible = this.webViewContainer.getVisibility() == 0;
            if (show == alreadyVisible) {
               return false;
            }
         }

         if (show) {
            this.webViewVisible = true;
            this.showDialog();
            UniWebViewManager.getInstance().addShowingDialog(this);
            if (this.showSpinnerWhileLoading && this.isLoading) {
               this.showSpinner();
            }
         } else {
            this.webViewVisible = false;
            InputMethodManager inputMethodManager = (InputMethodManager)this.activity.getSystemService("input_method");
            inputMethodManager.hideSoftInputFromWindow(this.uniWebView.getWindowToken(), 0);
            this.hideSpinner();
         }

         if (!fade && edge == 0) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
               public void run() {
                  UniWebViewDialog.this.finishShowDialog(show, identifier);
               }
            }, 1L);
         } else {
            this.animatedShow(show, fade, edge, duration, identifier);
         }

         return true;
      }
   }

   private void animatedShow(final boolean show, boolean fade, int edge, float duration, final String identifier) {
      this.animating = true;
      AnimationSet set = new AnimationSet(false);
      int durationMillisecond = (int)(duration * 1000.0F);
      Animation fadeAnimation = this.fadeAnimation(show, fade, durationMillisecond);
      if (fadeAnimation != null) {
         set.addAnimation(fadeAnimation);
      }

      Animation moveAnimation = this.moveAnimation(show, edge, durationMillisecond);
      if (moveAnimation != null) {
         set.addAnimation(moveAnimation);
      }

      this.webViewContainer.startAnimation(set);
      Handler handler = new Handler();
      handler.postDelayed(new Runnable() {
         public void run() {
            UniWebViewDialog.this.animating = false;
            UniWebViewDialog.this.webViewContainer.clearAnimation();
            UniWebViewDialog.this.finishShowDialog(show, identifier);
         }
      }, (long)durationMillisecond);
   }

   private void finishShowDialog(boolean show, String identifier) {
      if (show) {
         this.listener.onShowTransitionFinished(this, identifier);
      } else {
         this.webViewContainer.setVisibility(8);
         this.listener.onHideTransitionFinished(this, identifier);
      }

   }

   boolean animateTo(final int x, final int y, final int width, final int height, float duration, float delay, final String identifier) {
      if (this.animating) {
         Logger.getInstance().critical("Trying to animate but another transition animation is not finished yet. Ignore this one.");
         return false;
      } else {
         this.animating = true;
         int durationMillisecond = (int)(duration * 1000.0F);
         int delayMillisecond = (int)(delay * 1000.0F);
         AnimationSet set = new AnimationSet(false);
         set.addAnimation(this.moveToAnimation(x, y, durationMillisecond, delayMillisecond));
         set.addAnimation(this.sizeToAnimation(width, height, durationMillisecond, delayMillisecond));
         this.webViewContainer.startAnimation(set);
         Handler handler = new Handler();
         handler.postDelayed(new Runnable() {
            public void run() {
               UniWebViewDialog.this.animating = false;
               UniWebViewDialog.this.setFrame(x, y, width, height);
               UniWebViewDialog.this.webViewContainer.clearAnimation();
               UniWebViewDialog.this.listener.onAnimationFinished(UniWebViewDialog.this, identifier);
            }
         }, (long)(durationMillisecond + delayMillisecond));
         return true;
      }
   }

   void load(String url) {
      Logger.getInstance().info("UniWebView will load url: " + url + ". With headers: " + this.headers.toString());
      boolean result = this.shouldOverride(url, false);
      if (!result) {
         this.uniWebView.loadUrl(url, this.headers);
      }

   }

   void stop() {
      this.uniWebView.stopLoading();
      this.uniWebView.getClient().setUserStopped(true);
   }

   boolean shouldOverride(String url, boolean checkExternal) {
      Logger logger = Logger.getInstance();
      logger.info("shouldOverrideUrlLoading for: " + url);
      URLLoadingResponser responser = new URLLoadingResponser(this.activity, this, url);
      if (responser.canResponseBuiltinScheme()) {
         logger.debug("Url handled internally in UniWebView.");
         return true;
      } else if (responser.handleWithIntent()) {
         logger.debug("Url handled by intent.");
         return true;
      } else if (responser.canResponseDefinedScheme()) {
         logger.debug("Url redirected to Unity: " + url);
         this.listener.onSendMessageReceived(this, url);
         return true;
      } else if (checkExternal && this.openUrlExternal(url)) {
         logger.debug("Url redirected to Unity: " + url);
         return true;
      } else if (responser.handleWithThirdPartyApp()) {
         logger.debug("Url handled by a third party app: " + url);
         return true;
      } else {
         logger.debug("Url is opening without overridden: " + url);
         this.uniWebView.getClient().prepareLoadingStates();
         return false;
      }
   }

   void loadHTMLString(String html, String baseUrl) {
      Logger.getInstance().info("UniWebView will load html string with base url: " + baseUrl);
      Logger.getInstance().verbose("Input html content: \n" + html);
      this.uniWebView.getClient().prepareLoadingStates();
      this.uniWebView.loadDataWithBaseURL(baseUrl, html, "text/html", "UTF-8", (String)null);
   }

   void addJavaScript(String jsString, final String identifier) {
      if (jsString == null) {
         Logger.getInstance().critical("Trying to add null as js string. Aborting...");
      } else {
         Logger.getInstance().debug("Adding javascript string to web view. Requesting string: " + jsString);
         this.uniWebView.evaluateJavascript(jsString, new ValueCallback<String>() {
            public void onReceiveValue(String value) {
               Logger.getInstance().info("Receive a call back of adding js interface: " + value);
               UniWebViewResultPayload payload;
               if (value.equalsIgnoreCase("null")) {
                  payload = new UniWebViewResultPayload(identifier, "0", "");
                  UniWebViewDialog.this.listener.onAddJavaScriptFinished(UniWebViewDialog.this, payload.toString());
               } else {
                  payload = new UniWebViewResultPayload(identifier, "-1", value);
                  UniWebViewDialog.this.listener.onAddJavaScriptFinished(UniWebViewDialog.this, payload.toString());
               }

            }
         });
      }
   }

   void evaluateJavaScript(String jsString, final String identifier) {
      if (jsString == null) {
         Logger.getInstance().critical("Trying to evaluate null as js string. Aborting...");
      } else {
         Logger.getInstance().debug("Evaluating javascript string in web view. Requesting string: " + jsString);
         this.uniWebView.evaluateJavascript(jsString, new ValueCallback<String>() {
            public void onReceiveValue(String value) {
               Logger.getInstance().info("Receive a call back of evaluating js interface: " + value);
               UniWebViewResultPayload payload;
               if (value.equalsIgnoreCase("null")) {
                  payload = new UniWebViewResultPayload(identifier, "0", "");
                  UniWebViewDialog.this.listener.onJavaScriptFinished(UniWebViewDialog.this, payload.toString());
               } else {
                  value = value.replaceAll("^\"|\"$", "");
                  value = UniWebViewDialog.this.removeUTFCharacters(value).toString();
                  value = UniWebViewDialog.this.unescapeJavaString(value);
                  payload = new UniWebViewResultPayload(identifier, "0", value);
                  UniWebViewDialog.this.listener.onJavaScriptFinished(UniWebViewDialog.this, payload.toString());
               }

            }
         });
      }
   }

   boolean goBack() {
      WebView popupWebView = this.chromeClient.getPopUpWebView();
      if (popupWebView != null) {
         if (popupWebView.canGoBack()) {
            popupWebView.goBack();
         } else {
            popupWebView.evaluateJavascript("window.close()", new ValueCallback<String>() {
               public void onReceiveValue(String value) {
               }
            });
         }

         return true;
      } else if (this.uniWebView.canGoBack()) {
         this.uniWebView.goBack();
         return true;
      } else {
         return false;
      }
   }

   boolean goForward() {
      if (this.uniWebView.canGoForward()) {
         this.uniWebView.goForward();
         return true;
      } else {
         return false;
      }
   }

   void destroy() {
      this.uniWebView.loadUrl("about:blank");
      UniWebViewManager.getInstance().removeShowingDialog(this);
      CookieManager.getInstance().flush();
      this.dismiss();
   }

   void goBackground() {
      if (this.isLoading) {
         this.loadingInterrupted = true;
         this.uniWebView.stopLoading();
      }

      if (this.webViewShowing) {
         AlertDialog alert = this.chromeClient.getAlertDialog();
         if (alert != null) {
            alert.hide();
         }

         this.hide();
         this.uniWebView.onPause();
      }

   }

   void goForeground() {
      if (this.loadingInterrupted) {
         this.uniWebView.reload();
         this.loadingInterrupted = false;
      }

      if (this.webViewShowing) {
         this.show();
         AlertDialog alert = this.chromeClient.getAlertDialog();
         if (alert != null) {
            alert.show();
         }

         this.uniWebView.onResume();
      }

   }

   void setSpinnerText(String spinnerText) {
      if (spinnerText != null) {
         this.spinnerText = spinnerText;
      } else {
         this.spinnerText = this.getContext().getResources().getString(string.LOADING);
      }

      this.spinner.setMessage(this.spinnerText);
   }

   void showSpinner() {
      Logger.getInstance().verbose("Show spinner for loading.");
      this.spinner.show();
   }

   void hideSpinner() {
      Logger.getInstance().verbose("Hide spinner.");
      this.spinner.hide();
   }

   void setBackgroundColor(float r, float g, float b, float a) {
      int redInt = (int)(r * 255.0F);
      int greenInt = (int)(g * 255.0F);
      int blueInt = (int)(b * 255.0F);
      int alphaInt = (int)(a * 255.0F);
      int backgroundColor = Color.argb(alphaInt, redInt, greenInt, blueInt);
      this.webViewContainer.setBackground(new ColorDrawable(backgroundColor));
      this.videoContainer.setBackground(new ColorDrawable(backgroundColor));
   }

   void setOpenLinksInExternalBrowser(boolean openLinksInExternalBrowser) {
      this.openLinksInExternalBrowser = openLinksInExternalBrowser;
   }

   private void prepareWindow() {
      Logger.getInstance().verbose("Preparing window layout for web view dialog.");
      Window window = this.getWindow();
      window.setBackgroundDrawable(new ColorDrawable(0));
      window.addFlags(32);
      window.setSoftInputMode(16);
      window.setFlags(1024, 1024);
      window.setFlags(16777216, 16777216);
      if (VERSION.SDK_INT >= 28) {
         int mode = this.activity.getWindow().getAttributes().layoutInDisplayCutoutMode;
         LayoutParams params = this.getWindow().getAttributes();
         params.layoutInDisplayCutoutMode = mode;
         this.getWindow().setAttributes(params);
      }

   }

   private void addWebViewContent() {
      this.webViewContainer = new FrameLayout(this.getContext());
      this.webViewContainer.setVisibility(0);
      this.videoContainer = new FrameLayout(this.getContext());
      this.videoContainer.setVisibility(4);
      this.uniWebView = new UniWebView(this.activity);
      this.uniWebView = new UniWebView(this.activity) {
         public HashMap<String, String> getCustomizeHeaders() {
            Logger.getInstance().critical("Get header!!!");
            return UniWebViewDialog.this.getHeaders();
         }
      };
      this.uniWebView.setClient(new UniWebViewClient(this));
      this.chromeClient = new UniWebViewChromeClient(this.activity, this.webViewContainer, this.videoContainer, (View)null, this.uniWebView, this);
      this.uniWebView.setWebChromeClient(this.chromeClient);
      this.uniWebView.setDownloadListener(new DownloadListener() {
         public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
            Logger.getInstance().info("UniWebView onDownloadStart for url: " + url);
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse(url));
            UniWebViewDialog.this.activity.startActivity(intent);
         }
      });
      this.uniWebView.setVisibility(0);
      this.uniWebView.setBackgroundColor(0);
      this.spinner = new ProgressDialog(this.getContext());
      this.spinner.setCanceledOnTouchOutside(true);
      this.spinner.requestWindowFeature(1);
      this.setSpinnerText(this.spinnerText);
      android.view.ViewGroup.LayoutParams videoParams = new android.view.ViewGroup.LayoutParams(-1, -1);
      this.addContentView(this.videoContainer, videoParams);
      android.view.ViewGroup.LayoutParams webParams = new android.view.ViewGroup.LayoutParams(this.width, this.height);
      this.webViewContainer.setX((float)this.x);
      this.webViewContainer.setY((float)this.y);
      this.addContentView(this.webViewContainer, webParams);
      this.webViewContainer.addView(this.uniWebView);
      this.setBackgroundColor(1.0F, 1.0F, 1.0F, 1.0F);
   }

   boolean openUrlExternal(String url) {
      if (this.uniWebView != null && this.uniWebView.getHitTestResult() != null) {
         if (!this.openLinksInExternalBrowser) {
            Logger.getInstance().verbose("UniWebView should open links in current web view.");
            return false;
         } else if (this.uniWebView.getHitTestResult().getType() == 0) {
            Logger.getInstance().debug("UniWebView getHitTestResult unknown. Do not open url externally.");
            return false;
         } else {
            Logger.getInstance().verbose("UniWebView is opening links in external browser.");
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse(url));
            this.getContext().startActivity(intent);
            return true;
         }
      } else {
         Logger.getInstance().critical("Failed to open url due to dialog or webview being null. Url: " + url);
         return false;
      }
   }

   void hideSystemUI() {
      View decorView = this.getWindow().getDecorView();
      short updatedUIOptions;
      if (this.immersiveMode) {
         updatedUIOptions = 5894;
      } else {
         updatedUIOptions = 4;
      }

      decorView.setSystemUiVisibility(updatedUIOptions);
   }

   void setLoading(boolean loading) {
      this.isLoading = loading;
   }

   boolean isShowSpinnerWhileLoading() {
      return this.showSpinnerWhileLoading;
   }

   void setShowSpinnerWhileLoading(boolean showSpinnerWhileLoading) {
      this.showSpinnerWhileLoading = showSpinnerWhileLoading;
   }

   void print() {
      PrintManager printManager = (PrintManager)this.getContext().getSystemService("print");
      PrintDocumentAdapter printAdapter = this.getWebView().createPrintDocumentAdapter(this.uniWebView.getUrl());
      printManager.print("UniWebView Printing", printAdapter, (new Builder()).build());
   }

   UniWebView getWebView() {
      return this.uniWebView;
   }

   HashSet<String> getSslExceptionDomains() {
      return this.sslExceptionDomains;
   }

   HashSet<String> getPermissionTrustDomains() {
      return this.permissionTrustDomains;
   }

   boolean getImmersiveMode() {
      return this.immersiveMode;
   }

   private void setKeyboardVisibilityListener(final OnKeyboardVisibilityListener onKeyboardVisibilityListener) {
      final View parentView = ((ViewGroup)this.findViewById(16908290)).getChildAt(0);
      parentView.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
         private boolean alreadyOpen;
         private final int defaultKeyboardHeightDP = 100;
         private final int EstimatedKeyboardDP;
         private final Rect rect;

         {
            this.EstimatedKeyboardDP = 100 + (VERSION.SDK_INT >= 21 ? 48 : 0);
            this.rect = new Rect();
         }

         public void onGlobalLayout() {
            int estimatedKeyboardHeight = (int)TypedValue.applyDimension(1, (float)this.EstimatedKeyboardDP, parentView.getResources().getDisplayMetrics());
            parentView.getWindowVisibleDisplayFrame(this.rect);
            int heightDiff = parentView.getRootView().getHeight() - (this.rect.bottom - this.rect.top);
            boolean isShown = heightDiff >= estimatedKeyboardHeight;
            if (isShown != this.alreadyOpen) {
               this.alreadyOpen = isShown;
               onKeyboardVisibilityListener.onVisibilityChanged(isShown);
            }
         }
      });
   }

   public void onVisibilityChanged(boolean visible) {
      if (!visible) {
         Handler handler = new Handler();
         handler.post(new Runnable() {
            public void run() {
               UniWebViewDialog.this.hideSystemUI();
            }
         });
      }

   }

   void setImmersiveMode(boolean immersiveMode) {
      this.immersiveMode = immersiveMode;
   }

   void setBouncesEnabled(boolean enable) {
      if (enable) {
         this.uniWebView.setOverScrollMode(0);
      } else {
         this.uniWebView.setOverScrollMode(2);
      }

   }

   void setCalloutEnabled(boolean enabled) {
      this.uniWebView.calloutEnabled = enabled;
   }

   void setBackButtonEnabled(boolean backButtonEnabled) {
      this.backButtonEnabled = backButtonEnabled;
   }

   void setZoomEnabled(boolean enable) {
      this.uniWebView.getSettings().setBuiltInZoomControls(enable);
   }

   float getWebViewAlpha() {
      return this.webViewAlpha;
   }

   void setWebViewAlpha(float alpha, boolean actually) {
      float a = alpha > 1.0F ? 1.0F : (alpha < 0.0F ? 0.0F : alpha);
      this.webViewAlpha = a;
      if (actually) {
         this.webViewContainer.setAlpha(a);
      }

   }

   void setHeaderField(String key, String value) {
      if (key != null && key.length() != 0) {
         if (value == null) {
            this.headers.remove(key);
         } else {
            this.headers.put(key, value);
         }

      } else {
         Logger.getInstance().critical("Trying to set null or empty key for header field. Please check you have set correct key.");
      }
   }

   void setSupportMultipleWindows(boolean enabled) {
      this.uniWebView.getSettings().setSupportMultipleWindows(enabled);
   }

   void setDefaultFontSize(int size) {
      float scale = this.activity.getResources().getConfiguration().fontScale;
      this.uniWebView.getSettings().setDefaultFontSize(Math.round((float)size / scale));
   }

   HashMap<String, String> getHeaders() {
      return this.headers;
   }

   void clearHttpAuthUsernamePassword(String host, String realm) {
      this.getWebView().setHttpAuthUsernamePassword(host, realm, (String)null, (String)null);
   }

   void scrollTo(int x, int y, boolean animated) {
      if (animated) {
         ObjectAnimator animX = ObjectAnimator.ofInt(this.uniWebView, "scrollX", new int[]{this.uniWebView.getScrollX(), x});
         ObjectAnimator animY = ObjectAnimator.ofInt(this.uniWebView, "scrollY", new int[]{this.uniWebView.getScrollY(), y});
         AnimatorSet animatorSet = new AnimatorSet();
         animatorSet.playTogether(new Animator[]{animX, animY});
         animatorSet.setDuration(400L).start();
      } else {
         this.uniWebView.scrollTo(x, y);
      }

   }

   private void showDialog() {
      if (this.webViewShowing) {
         this.webViewContainer.setVisibility(0);
      } else {
         this.webViewShowing = true;
         this.show();
      }

   }

   private Point displaySize() {
      Display display = this.getWindow().getWindowManager().getDefaultDisplay();
      Point size = new Point();
      if (this.immersiveMode) {
         display.getRealSize(size);
         return size;
      } else {
         display.getSize(size);
         return size;
      }
   }

   private Animation fadeAnimation(boolean show, boolean fade, int durationMillisecond) {
      if (!fade) {
         return null;
      } else {
         float startAlpha = show ? 0.0F : this.getWebViewAlpha();
         float endAlpha = show ? this.getWebViewAlpha() : 0.0F;
         Animation animation = new AlphaAnimation(startAlpha, endAlpha);
         animation.setFillAfter(true);
         animation.setDuration((long)durationMillisecond);
         return animation;
      }
   }

   private Animation moveAnimation(boolean show, int edge, int durationMillisecond) {
      if (edge == 0) {
         return null;
      } else {
         Point size = this.displaySize();
         int xValue;
         int yValue;
         if (edge == 1) {
            xValue = 0;
            yValue = -size.y;
         } else if (edge == 2) {
            xValue = -size.x;
            yValue = 0;
         } else if (edge == 3) {
            xValue = 0;
            yValue = size.y;
         } else {
            if (edge != 4) {
               return null;
            }

            xValue = size.x;
            yValue = 0;
         }

         Animation animation = new TranslateAnimation(show ? (float)xValue : 0.0F, show ? 0.0F : (float)xValue, show ? (float)yValue : 0.0F, show ? 0.0F : (float)yValue);
         animation.setFillAfter(true);
         animation.setDuration((long)durationMillisecond);
         return animation;
      }
   }

   private Animation moveToAnimation(int x, int y, int durationMillisecond, int delayMillisecond) {
      Animation animation = new TranslateAnimation(0.0F, (float)(x - this.x), 0.0F, (float)(y - this.y));
      animation.setFillAfter(true);
      animation.setDuration((long)durationMillisecond);
      animation.setStartOffset((long)delayMillisecond);
      return animation;
   }

   private Animation sizeToAnimation(int width, int height, int durationMillisecond, int delayMillisecond) {
      Animation animation = new ResizeAnimation(this.webViewContainer, this.width, width, this.height, height);
      animation.setFillAfter(true);
      animation.setDuration((long)durationMillisecond);
      animation.setStartOffset((long)delayMillisecond);
      return animation;
   }

   private StringBuffer removeUTFCharacters(String data) {
      Pattern p = Pattern.compile("\\\\u(\\p{XDigit}{4})");
      Matcher m = p.matcher(data);
      StringBuffer buf = new StringBuffer(data.length());

      while(m.find()) {
         String ch = String.valueOf((char)Integer.parseInt(m.group(1), 16));
         m.appendReplacement(buf, Matcher.quoteReplacement(ch));
      }

      m.appendTail(buf);
      return buf;
   }

   private String unescapeJavaString(String st) {
      StringBuilder sb = new StringBuilder(st.length());

      for(int i = 0; i < st.length(); ++i) {
         char ch = st.charAt(i);
         if (ch == '\\') {
            char nextChar = i == st.length() - 1 ? 92 : st.charAt(i + 1);
            if (nextChar >= '0' && nextChar <= '7') {
               String code = "" + nextChar;
               ++i;
               if (i < st.length() - 1 && st.charAt(i + 1) >= '0' && st.charAt(i + 1) <= '7') {
                  code = code + st.charAt(i + 1);
                  ++i;
                  if (i < st.length() - 1 && st.charAt(i + 1) >= '0' && st.charAt(i + 1) <= '7') {
                     code = code + st.charAt(i + 1);
                     ++i;
                  }
               }

               sb.append((char)Integer.parseInt(code, 8));
               continue;
            }

            switch(nextChar) {
            case '"':
               ch = '"';
               break;
            case '\'':
               ch = '\'';
               break;
            case '\\':
               ch = '\\';
               break;
            case 'b':
               ch = '\b';
               break;
            case 'f':
               ch = '\f';
               break;
            case 'n':
               ch = '\n';
               break;
            case 'r':
               ch = '\r';
               break;
            case 't':
               ch = '\t';
               break;
            case 'u':
               if (i < st.length() - 5) {
                  int code = Integer.parseInt("" + st.charAt(i + 2) + st.charAt(i + 3) + st.charAt(i + 4) + st.charAt(i + 5), 16);
                  sb.append(Character.toChars(code));
                  i += 5;
                  continue;
               }

               ch = 'u';
            }

            ++i;
         }

         sb.append(ch);
      }

      return sb.toString();
   }

   public boolean getShowHTTPAuthPopUpWindow() {
      return this.showHTTPAuthPopUpWindow;
   }

   public void setShowHTTPAuthPopUpWindow(boolean showHTTPAuthPopUpWindow) {
      this.showHTTPAuthPopUpWindow = showHTTPAuthPopUpWindow;
   }

   public boolean isUserInteractionEnabled() {
      return this.userInteractionEnabled;
   }

   public void setUserInteractionEnabled(boolean userInteractionEnabled) {
      this.userInteractionEnabled = userInteractionEnabled;
   }

   interface DialogListener {
      void onPageFinished(UniWebViewDialog var1, int var2, String var3);

      void onPageStarted(UniWebViewDialog var1, String var2);

      void onReceivedError(UniWebViewDialog var1, int var2, String var3, String var4);

      boolean shouldOverrideUrlLoading(UniWebViewDialog var1, String var2);

      void onSendMessageReceived(UniWebViewDialog var1, String var2);

      void onDialogClosedByBackButton(UniWebViewDialog var1);

      void onDialogKeyDown(UniWebViewDialog var1, int var2);

      void onDialogClose(UniWebViewDialog var1);

      void onAddJavaScriptFinished(UniWebViewDialog var1, String var2);

      void onJavaScriptFinished(UniWebViewDialog var1, String var2);

      void onAnimationFinished(UniWebViewDialog var1, String var2);

      void onShowTransitionFinished(UniWebViewDialog var1, String var2);

      void onHideTransitionFinished(UniWebViewDialog var1, String var2);
   }
}
