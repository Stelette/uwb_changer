package com.onevcat.uniwebview;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Build.VERSION;
import android.webkit.WebChromeClient.FileChooserParams;
import com.onevcat.uniwebview.R.string;
import java.io.File;
import java.io.IOException;

public class UniWebViewFileChooserActivity extends Activity {
   private static final int FILECHOOSER_RESULTCODE = 19238467;
   private static final int WRITE_EXTERNAL_STORAGE_REQUEST_CODE = 19238468;
   public static final String INTENT_CHROME_CLIENT_NAME = "chrome_client";

   private UniWebViewChromeClient getChromeClient() {
      String name = this.getIntent().getStringExtra("chrome_client");
      if (name == null) {
         Logger.getInstance().critical("The intent does not contain a extra name for chrome client. It should not happen.");
         return null;
      } else {
         Logger.getInstance().verbose("Getting chrome client with web view name: " + name);
         UniWebViewDialog dialog = UniWebViewManager.getInstance().getUniWebViewDialog(name);
         if (dialog == null) {
            Logger.getInstance().critical("Cannot get a correct chrome client. Error.");
            return null;
         } else {
            return dialog.getChromeClient();
         }
      }
   }

   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      Logger.getInstance().verbose("UniWebViewFileChooserActivity onCreate. Bound client: " + this.getChromeClient());
      if (VERSION.SDK_INT >= 23 && this.checkSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE") != 0) {
         this.requestPermissions(new String[]{"android.permission.WRITE_EXTERNAL_STORAGE"}, 19238468);
      } else {
         this.startFileChooserActivity();
      }

   }

   public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
      super.onRequestPermissionsResult(requestCode, permissions, grantResults);
      if (requestCode == 19238468) {
         if (grantResults[0] == 0) {
            this.startFileChooserActivity();
         } else {
            UniWebViewChromeClient client = this.getChromeClient();
            if (client == null) {
               Logger.getInstance().critical("Unexpected onRequestPermissionsResult since the chrome client has been already reset to null.");
            } else {
               client.receivedFileValue((Intent)null, false);
               this.finish();
            }
         }
      }

   }

   protected void onActivityResult(int requestCode, int resultCode, Intent data) {
      Logger.getInstance().verbose("File Chooser activity result: " + resultCode);
      if (requestCode == 19238467) {
         UniWebViewChromeClient client = this.getChromeClient();
         if (client == null) {
            Logger.getInstance().critical("Unexpected onActivityResult since the chrome client has been already reset to null.");
         } else if (resultCode == -1) {
            Logger.getInstance().info("File chooser got a file. : " + data);
            client.receivedFileValue(data, true);
         } else {
            Logger.getInstance().critical("File chooser failed to get a file. Result code: " + resultCode);
            client.receivedFileValue((Intent)null, false);
         }
      }

      super.onActivityResult(requestCode, resultCode, data);
      this.finish();
   }

   private void startFileChooserActivity() {
      Intent takePictureIntent = new Intent("android.media.action.IMAGE_CAPTURE");
      UniWebViewChromeClient client = this.getChromeClient();
      if (client == null) {
         Logger.getInstance().critical("Unexpected startFileChooserActivity since the chrome client has been already reset to null.");
         Logger.getInstance().critical("There is not related chrome client with this file chooser. It might be an Android system issue and the file choosing process is terminated.");
      } else {
         if (takePictureIntent.resolveActivity(this.getPackageManager()) != null) {
            Logger.getInstance().verbose("Found an activity for taking photo. Try to get image.");
            File photoFile = null;

            try {
               photoFile = client.createImageFile();
               Logger.getInstance().verbose("photoFile: " + photoFile.getAbsolutePath());
               takePictureIntent.putExtra("PhotoPath", client.getCameraPhotoPath());
            } catch (IOException var7) {
               Logger.getInstance().critical("Error while creating image file. Exception: " + var7);
            }

            if (photoFile != null) {
               client.setCameraPhotoPath("file:" + photoFile.getAbsolutePath());
               takePictureIntent.putExtra("output", Uri.fromFile(photoFile));
            } else {
               takePictureIntent = null;
            }
         }

         Intent contentSelectionIntent = new Intent("android.intent.action.OPEN_DOCUMENT");
         contentSelectionIntent.addCategory("android.intent.category.OPENABLE");
         FileChooserParams params = client.getFileChooserParams();
         contentSelectionIntent.setType("*/*");
         if (params != null && params.getAcceptTypes() != null && params.getAcceptTypes().length >= 1 && !params.getAcceptTypes()[0].equals("")) {
            contentSelectionIntent.putExtra("android.intent.extra.MIME_TYPES", params.getAcceptTypes());
         }

         contentSelectionIntent.putExtra("android.intent.extra.ALLOW_MULTIPLE", true);
         Intent[] intentArray;
         if (takePictureIntent != null) {
            intentArray = new Intent[]{takePictureIntent};
         } else {
            intentArray = new Intent[0];
         }

         Intent chooserIntent = new Intent("android.intent.action.CHOOSER");
         chooserIntent.putExtra("android.intent.extra.TITLE", this.getResources().getString(string.CHOOSE_IMAGE));
         chooserIntent.putExtra("android.intent.extra.INTENT", contentSelectionIntent);
         chooserIntent.putExtra("android.intent.extra.INITIAL_INTENTS", intentArray);
         this.startActivityForResult(chooserIntent, 19238467);
      }
   }
}
