package com.onevcat.uniwebview;

import android.app.Activity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.unity3d.player.UnityPlayer;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class UniWebViewInterface {
   public static void setLogLevel(int level) {
      Logger.getInstance().setLevel(level);
   }

   public static void prepare() {
      final CountDownLatch latch = new CountDownLatch(1);
      final Activity activity = _getUnityActivity();
      activity.runOnUiThread(new Runnable() {
         public void run() {
            UniWebViewDialog.defaultUserAgent = WebSettings.getDefaultUserAgent(activity);
            latch.countDown();
         }
      });

      try {
         latch.await();
      } catch (InterruptedException var3) {
         var3.printStackTrace();
      }

   }

   public static void init(final String name, final int x, final int y, final int width, final int height) {
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         public void run() {
            UniWebViewDialog.DialogListener listener = new UniWebViewDialog.DialogListener() {
               public void onPageFinished(UniWebViewDialog dialog, int statusCode, String url) {
                  Logger.getInstance().info("onPageFinished: " + url);
                  UniWebViewResultPayload payload = new UniWebViewResultPayload("", Integer.toString(statusCode), url);
                  UniWebViewInterface._sendMessage(name, "PageFinished", payload.toString());
               }

               public void onPageStarted(UniWebViewDialog dialog, String url) {
                  Logger.getInstance().info("onPageStarted: " + url);
                  UniWebViewInterface._sendMessage(name, "PageStarted", url);
               }

               public void onReceivedError(UniWebViewDialog dialog, int errorCode, String description, String failingUrl) {
                  Logger.getInstance().critical("onReceivedError: " + failingUrl + " Error Code: " + errorCode + "Error Description: " + description);
                  UniWebViewResultPayload payload = new UniWebViewResultPayload("", Integer.toString(errorCode), description);
                  UniWebViewInterface._sendMessage(name, "PageErrorReceived", payload.toString());
               }

               public boolean shouldOverrideUrlLoading(UniWebViewDialog dialog, String url) {
                  return dialog.shouldOverride(url, true);
               }

               public void onSendMessageReceived(UniWebViewDialog dialog, String url) {
                  UniWebViewInterface._sendMessage(name, "MessageReceived", url);
               }

               public void onDialogClosedByBackButton(UniWebViewDialog dialog) {
                  Logger.getInstance().info("onDialogClosedByBackButton");
                  UniWebViewInterface._sendMessage(name, "WebViewDone", "");
               }

               public void onDialogKeyDown(UniWebViewDialog dialog, int keyCode) {
                  Logger.getInstance().info("onDialogKeyDown, key: " + keyCode);
                  UniWebViewInterface._sendMessage(name, "WebViewKeyDown", Integer.toString(keyCode));
               }

               public void onDialogClose(UniWebViewDialog dialog) {
                  Logger.getInstance().info("onDialogClose");
                  UniWebViewManager.getInstance().removeUniWebView(name);
               }

               public void onAddJavaScriptFinished(UniWebViewDialog uniWebViewDialog, String value) {
                  Logger.getInstance().info("onAddJavaScriptFinished");
                  Logger.getInstance().debug("js result: " + value);
                  UniWebViewInterface._sendMessage(name, "AddJavaScriptFinished", value);
               }

               public void onJavaScriptFinished(UniWebViewDialog dialog, String result) {
                  Logger.getInstance().info("onEvalJavaScriptFinished");
                  Logger.getInstance().debug("js result: " + result);
                  UniWebViewInterface._sendMessage(name, "EvalJavaScriptFinished", result);
               }

               public void onAnimationFinished(UniWebViewDialog dialog, String identifier) {
                  Logger.getInstance().info("onAnimationFinished, animation id: " + identifier);
                  UniWebViewInterface._sendMessage(name, "AnimateToFinished", identifier);
               }

               public void onShowTransitionFinished(UniWebViewDialog dialog, String identifier) {
                  Logger.getInstance().info("onShowTransitionFinished");
                  UniWebViewInterface._sendMessage(name, "ShowTransitionFinished", identifier);
               }

               public void onHideTransitionFinished(UniWebViewDialog dialog, String identifier) {
                  Logger.getInstance().info("onHideTransitionFinished");
                  UniWebViewInterface._sendMessage(name, "HideTransitionFinished", identifier);
               }
            };
            Logger.getInstance().info("Interface init: " + name);
            UniWebViewDialog dialog = new UniWebViewDialog(UniWebViewInterface._getUnityActivity(), listener);
            dialog.setFrame(x, y, width, height);
            UniWebViewManager.getInstance().setUniWebView(name, dialog);
         }
      });
   }

   public static void destroy(String name) {
      Logger.getInstance().info("Interface destroy web view" + name);
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.destroy();
         }
      });
   }

   public static void load(String name, final String url) {
      Logger.getInstance().info("Interface load: " + url);
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         public void runWith(UniWebViewDialog dialog) {
            dialog.load(url);
         }
      });
   }

   public static void loadHTMLString(String name, final String html, final String baseUrl) {
      Logger.getInstance().info("Interface loadHTMLString");
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         public void runWith(UniWebViewDialog dialog) {
            dialog.loadHTMLString(html, baseUrl);
         }
      });
   }

   public static void reload(String name) {
      Logger.getInstance().info("Interface reload");
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         public void runWith(UniWebViewDialog dialog) {
            dialog.getWebView().reload();
         }
      });
   }

   public static void stop(final String name) {
      Logger.getInstance().info("Interface stop");
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         public void run() {
            UniWebViewDialog dialog = UniWebViewManager.getInstance().getUniWebViewDialog(name);
            if (dialog != null) {
               dialog.stop();
            }

         }
      });
   }

   public static String getUrl(String name) {
      Logger.getInstance().info("Interface getUrl");
      UniWebViewDialog dialog = UniWebViewManager.getInstance().getUniWebViewDialog(name);
      final CountDownLatch latch = new CountDownLatch(1);
      final String[] result = new String[]{""};
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            result[0] = dialog.getUrl();
            latch.countDown();
         }
      });

      try {
         latch.await(100L, TimeUnit.MILLISECONDS);
      } catch (InterruptedException var5) {
         var5.printStackTrace();
      }

      return result[0];
   }

   public static void setFrame(String name, final int x, final int y, final int width, final int height) {
      Logger.getInstance().info(String.format(Locale.US, "Interface setFrame: {%d, %d, %d, %d}", x, y, width, height));
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         public void runWith(UniWebViewDialog dialog) {
            dialog.setFrame(x, y, width, height);
         }
      });
   }

   public static void setPosition(String name, final int x, final int y) {
      Logger.getInstance().info(String.format(Locale.US, "Interface setPosition: {%d, %d}", x, y));
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.setPosition(x, y);
         }
      });
   }

   public static void setSize(String name, final int width, final int height) {
      Logger.getInstance().info(String.format(Locale.US, "Interface setSize: {%d, %d}", width, height));
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.setSize(width, height);
         }
      });
   }

   public static boolean show(String name, final boolean fade, final int edge, final float duration, final String identifier) {
      Logger.getInstance().info("Interface show");
      final CountDownLatch latch = new CountDownLatch(1);
      final boolean[] result = new boolean[1];
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         public void runWith(UniWebViewDialog dialog) {
            result[0] = dialog.setShow(true, fade, edge, duration, identifier);
            latch.countDown();
         }
      });

      try {
         latch.await(100L, TimeUnit.MILLISECONDS);
      } catch (InterruptedException var8) {
         var8.printStackTrace();
      }

      return result[0];
   }

   public static boolean hide(String name, final boolean fade, final int edge, final float duration, final String identifier) {
      Logger.getInstance().info("Interface hide");
      final CountDownLatch latch = new CountDownLatch(1);
      final boolean[] result = new boolean[1];
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            result[0] = dialog.setShow(false, fade, edge, duration, identifier);
            latch.countDown();
         }
      });

      try {
         latch.await(100L, TimeUnit.MILLISECONDS);
      } catch (InterruptedException var8) {
         var8.printStackTrace();
      }

      return result[0];
   }

   public static boolean animateTo(String name, final int x, final int y, final int width, final int height, final float duration, final float delay, final String identifier) {
      Logger.getInstance().info(String.format(Locale.US, "Interface animateTo: {%d, %d, %d, %d}", x, y, width, height));
      final CountDownLatch latch = new CountDownLatch(1);
      final boolean[] result = new boolean[1];
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            result[0] = dialog.animateTo(x, y, width, height, duration, delay, identifier);
            latch.countDown();
         }
      });

      try {
         latch.await(100L, TimeUnit.MILLISECONDS);
      } catch (InterruptedException var11) {
         var11.printStackTrace();
      }

      return result[0];
   }

   public static void addJavaScript(String name, final String jsString, final String identifier) {
      Logger.getInstance().info("Interface addJavaScript");
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.addJavaScript(jsString, identifier);
         }
      });
   }

   public static void evaluateJavaScript(String name, final String jsString, final String identifier) {
      Logger.getInstance().info("Interface evaluateJavaScript");
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.evaluateJavaScript(jsString, identifier);
         }
      });
   }

   public static void addUrlScheme(String name, final String scheme) {
      Logger.getInstance().info("Interface addUrlScheme");
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            Logger.getInstance().verbose("Adding url scheme to web view: " + scheme);
            dialog.getSchemes().add(scheme);
         }
      });
   }

   public static void removeUrlScheme(String name, final String scheme) {
      Logger.getInstance().info("Interface addUrlScheme");
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            Logger.getInstance().verbose("Removing url scheme to web view: " + scheme);
            dialog.getSchemes().remove(scheme);
         }
      });
   }

   public static void setHeaderField(String name, final String key, final String value) {
      Logger.getInstance().info("Interface setHeaderField for key: " + key + ", value: " + value);
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.setHeaderField(key, value);
         }
      });
   }

   public static void setUserAgent(final String name, final String userAgent) {
      final Logger logger = Logger.getInstance();
      logger.info("Interface setUserAgent");
      UniWebViewDialog dialog = UniWebViewManager.getInstance().getUniWebViewDialog(name);
      UniWebViewDialog.setUserAgent(dialog, name, userAgent);
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            logger.verbose("Setting user agent string of web view to:" + userAgent);
            UniWebViewDialog.setUserAgent(dialog, name, userAgent);
         }
      });
   }

   public static String getUserAgent(String name) {
      Logger logger = Logger.getInstance();
      logger.info("Interface getUserAgent");
      UniWebViewDialog dialog = UniWebViewManager.getInstance().getUniWebViewDialog(name);
      return UniWebViewDialog.getUserAgent(dialog, name);
   }

   public static void setAllowAutoPlay(boolean flag) {
      Logger.getInstance().info("Interface setAllowAutoPlay: " + flag);
      UniWebView.setAllowAutoPlay(flag);
   }

   public static void setAllowJavaScriptOpenWindow(boolean flag) {
      Logger.getInstance().info("Interface setAllowJavaScriptOpenWindow: " + flag);
      UniWebView.setAllowJavaScriptOpenWindow(flag);
   }

   public static void setJavaScriptEnabled(boolean enabled) {
      Logger.getInstance().info("Interface setJavaScriptEnabled: " + enabled);
      UniWebView.setJavaScriptEnabled(enabled);
   }

   public static void cleanCache(String name) {
      Logger.getInstance().info("Interface cleanCache");
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.getWebView().clearCache(true);
         }
      });
   }

   public static void clearCookies() {
      Logger.getInstance().info("Interface clearCookies");
      UniWebView.clearCookies();
   }

   public static void setCookie(String url, String cookie) {
      Logger.getInstance().info("Interface setCookie");
      UniWebView.setCookie(url, cookie);
   }

   public static String getCookie(String url, String key) {
      Logger logger = Logger.getInstance();
      logger.info("Interface getCookie");
      return UniWebView.getCookie(url, key);
   }

   public static void clearHttpAuthUsernamePassword(final String host, final String realm) {
      Logger.getInstance().info("Interface clearHttpAuthUsernamePassword host: " + host + ", realm:" + realm);
      final Activity activity = _getUnityActivity();
      activity.runOnUiThread(new Runnable() {
         public void run() {
            UniWebViewDialog dialog = new UniWebViewDialog(activity, (UniWebViewDialog.DialogListener)null);
            dialog.clearHttpAuthUsernamePassword(host, realm);
         }
      });
   }

   public static void setBackgroundColor(String name, final float r, final float g, final float b, final float a) {
      Logger.getInstance().info(String.format(Locale.US, "Interface setBackgroundColor: {%f, %f, %f, %f}", r, g, b, a));
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.setBackgroundColor(r, g, b, a);
         }
      });
   }

   public static void setWebViewAlpha(String name, final float alpha) {
      Logger.getInstance().info("Interface setWebViewAlpha: " + alpha);
      UniWebViewDialog dialog = UniWebViewManager.getInstance().getUniWebViewDialog(name);
      if (dialog != null) {
         dialog.setWebViewAlpha(alpha, false);
      }

      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.setWebViewAlpha(alpha, true);
         }
      });
   }

   public static float getWebViewAlpha(String name) {
      Logger.getInstance().info("Interface getWebViewAlpha");
      UniWebViewDialog dialog = UniWebViewManager.getInstance().getUniWebViewDialog(name);
      return dialog != null ? dialog.getWebViewAlpha() : 1.0F;
   }

   public static void setShowSpinnerWhileLoading(String name, final boolean show) {
      Logger.getInstance().info("Interface setShowSpinnerWhenLoading: " + show);
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.setShowSpinnerWhileLoading(show);
         }
      });
   }

   public static void setSpinnerText(String name, final String text) {
      Logger.getInstance().info("Interface setSpinnerText: " + text);
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.setSpinnerText(text);
         }
      });
   }

   public static boolean canGoBack(String name) {
      Logger.getInstance().info("Interface canGoBack");
      final CountDownLatch latch = new CountDownLatch(1);
      final boolean[] result = new boolean[1];
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            result[0] = dialog.isCanGoBack();
            latch.countDown();
         }
      });

      try {
         latch.await(100L, TimeUnit.MILLISECONDS);
      } catch (InterruptedException var4) {
         var4.printStackTrace();
      }

      return result[0];
   }

   public static boolean canGoForward(String name) {
      Logger.getInstance().info("Interface canGoForward");
      final CountDownLatch latch = new CountDownLatch(1);
      final boolean[] result = new boolean[1];
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            result[0] = dialog.isCanGoForward();
            latch.countDown();
         }
      });

      try {
         latch.await(100L, TimeUnit.MILLISECONDS);
      } catch (InterruptedException var4) {
         var4.printStackTrace();
      }

      return result[0];
   }

   public static void goBack(String name) {
      Logger.getInstance().info("Interface goBack");
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.goBack();
         }
      });
   }

   public static void goForward(String name) {
      Logger.getInstance().info("Interface goForward");
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.goForward();
         }
      });
   }

   public static void setOpenLinksInExternalBrowser(String name, final boolean flag) {
      Logger.getInstance().info("Interface setOpenLinksInExternalBrowser: " + flag);
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.setOpenLinksInExternalBrowser(flag);
         }
      });
   }

   public static void setHorizontalScrollBarEnabled(String name, final boolean enabled) {
      Logger.getInstance().info("Interface setHorizontalScrollBarEnabled: " + enabled);
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.getWebView().setHorizontalScrollBarEnabled(enabled);
         }
      });
   }

   public static void setVerticalScrollBarEnabled(String name, final boolean enabled) {
      Logger.getInstance().info("Interface setVerticalScrollBarEnabled: " + enabled);
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.getWebView().setVerticalScrollBarEnabled(enabled);
         }
      });
   }

   public static void setBouncesEnabled(String name, final boolean enabled) {
      Logger.getInstance().info("Interface setBouncesEnabled");
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.setBouncesEnabled(enabled);
         }
      });
   }

   public static void setZoomEnabled(String name, final boolean enabled) {
      Logger.getInstance().info("Interface setZoomEnabled");
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.setZoomEnabled(enabled);
         }
      });
   }

   public static void addPermissionTrustDomain(String name, final String domain) {
      Logger.getInstance().info("Interface addPermissionTrustDomain: " + domain);
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.getPermissionTrustDomains().add(domain);
         }
      });
   }

   public static void removePermissionTrustDomain(String name, final String domain) {
      Logger.getInstance().info("Interface removePermissionTrustDomain: " + domain);
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.getPermissionTrustDomains().remove(domain);
         }
      });
   }

   public static void addSslExceptionDomain(String name, final String domain) {
      Logger.getInstance().info("Interface addSslExceptionDomain: " + domain);
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.getSslExceptionDomains().add(domain);
         }
      });
   }

   public static void removeSslExceptionDomain(String name, final String domain) {
      Logger.getInstance().info("Interface addSslExceptionDomain: " + domain);
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.getSslExceptionDomains().remove(domain);
         }
      });
   }

   public static void setBackButtonEnabled(String name, final boolean enabled) {
      Logger.getInstance().info("Interface setBackButtonEnabled");
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.setBackButtonEnabled(enabled);
         }
      });
   }

   public static void setUseWideViewPort(String name, final boolean use) {
      Logger.getInstance().info("Interface setUseWideViewPort");
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.getWebView().getSettings().setUseWideViewPort(use);
         }
      });
   }

   public static void setLoadWithOverviewMode(String name, final boolean overview) {
      Logger.getInstance().info("Interface setLoadWithOverviewMode");
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.getWebView().getSettings().setLoadWithOverviewMode(overview);
         }
      });
   }

   public static void setImmersiveModeEnabled(String name, final boolean enabled) {
      Logger.getInstance().info("Interface setImmersiveModeEnabled");
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.setImmersiveMode(enabled);
         }
      });
   }

   public static void setUserInteractionEnabled(String name, final boolean enabled) {
      Logger.getInstance().info("Interface setUserInteractionEnabled");
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.setUserInteractionEnabled(enabled);
         }
      });
   }

   public static void setWebContentsDebuggingEnabled(final boolean enabled) {
      Logger.getInstance().info("Interface setWebContentsDebuggingEnabled: " + enabled);
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable((String)null) {
         public void run() {
            WebView.setWebContentsDebuggingEnabled(enabled);
         }
      });
   }

   public static void setAllowHTTPAuthPopUpWindow(String name, final boolean enabled) {
      Logger.getInstance().info("Interface setAllowHTTPAuthPopUpWindow: " + enabled);
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.setShowHTTPAuthPopUpWindow(enabled);
         }
      });
   }

   public static void setCalloutEnabled(String name, final boolean enabled) {
      Logger.getInstance().info("Interface setCalloutEnabled: " + enabled);
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.setCalloutEnabled(enabled);
         }
      });
   }

   public static void setSupportMultipleWindows(String name, final boolean enabled) {
      Logger.getInstance().info("Interface setSupportMultipleWindows: " + enabled);
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.setSupportMultipleWindows(enabled);
         }
      });
   }

   public static void setDefaultFontSize(String name, final int size) {
      Logger.getInstance().info("setDefaultFontSize: " + size);
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.setDefaultFontSize(size);
         }
      });
   }

   public static void print(String name) {
      Logger.getInstance().info("Interface print");
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.print();
         }
      });
   }

   public static void showWebViewDialog(String name, final boolean show) {
      Logger.getInstance().info("Interface showWebViewDialog");
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            if (show) {
               dialog.goForeground();
               dialog.hideSystemUI();
            } else {
               dialog.goBackground();
            }

         }
      });
   }

   public static void scrollTo(String name, final int x, final int y, final boolean animated) {
      Logger.getInstance().info("Interface scrollTo (" + x + ", " + y + "), animated: " + animated);
      runSafelyOnUiThread(new UniWebViewInterface.DialogRunnable(name) {
         void runWith(UniWebViewDialog dialog) {
            dialog.scrollTo(x, y, animated);
         }
      });
   }

   public static float screenWidth() {
      View view = _getUnityActivity().findViewById(16908290);
      return (float)view.getWidth();
   }

   public static float screenHeight() {
      View view = _getUnityActivity().findViewById(16908290);
      return (float)view.getHeight();
   }

   private static void runSafelyOnUiThread(final UniWebViewInterface.DialogRunnable r) {
      _getUnityActivity().runOnUiThread(new Runnable() {
         public void run() {
            try {
               r.run();
            } catch (Exception var2) {
               Logger.getInstance().critical("UniWebView should run on UI thread: " + var2.getMessage());
            }

         }
      });
   }

   private static Activity _getUnityActivity() {
      return UnityPlayer.currentActivity;
   }

   private static void _sendMessage(String targetName, String methodName, String parameters) {
      String message = String.format("%s@%s@%s", targetName, methodName, parameters);
      UnityPlayer.UnitySendMessage("UniWebViewAndroidStaticListener", "OnJavaMessage", message);
   }

   private static class DialogRunnable implements Runnable {
      private String name;

      DialogRunnable(String name) {
         this.name = name;
      }

      void runWith(UniWebViewDialog dialog) {
         throw new UnsupportedOperationException();
      }

      public void run() {
         UniWebViewDialog dialog = UniWebViewManager.getInstance().getUniWebViewDialog(this.name);
         if (dialog != null) {
            this.runWith(dialog);
         } else {
            Logger.getInstance().critical("Did not find the correct web view dialog for name: " + this.name);
         }

      }
   }
}
