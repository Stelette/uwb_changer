package com.onevcat.uniwebview;

import android.app.Activity;
import android.view.MotionEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

class UniWebViewManager {
   private HashMap<String, UniWebViewDialog> webViewDialog = new HashMap();
   private ArrayList<UniWebViewDialog> showingDialogs = new ArrayList();
   private static UniWebViewManager instance = null;

   private UniWebViewManager() {
   }

   static UniWebViewManager getInstance() {
      if (instance == null) {
         instance = new UniWebViewManager();
      }

      return instance;
   }

   UniWebViewDialog getUniWebViewDialog(String name) {
      return name != null && name.length() != 0 && this.webViewDialog.containsKey(name) ? (UniWebViewDialog)this.webViewDialog.get(name) : null;
   }

   void removeUniWebView(String name) {
      if (this.webViewDialog.containsKey(name)) {
         Logger.getInstance().debug("Removing web view dialog from manager: " + name);
         this.webViewDialog.remove(name);
      }

   }

   void setUniWebView(String name, UniWebViewDialog webViewDialog) {
      Logger.getInstance().debug("Adding web view dialog to manager: " + name);
      webViewDialog.setWebViewName(name);
      this.webViewDialog.put(name, webViewDialog);
   }

   Collection<UniWebViewDialog> allDialogs() {
      return this.webViewDialog.values();
   }

   void addShowingDialog(UniWebViewDialog webViewDialog) {
      if (!this.showingDialogs.contains(webViewDialog)) {
         this.showingDialogs.add(webViewDialog);
      }

   }

   void removeShowingDialog(UniWebViewDialog webViewDialog) {
      this.showingDialogs.remove(webViewDialog);
   }

   boolean handleTouchEvent(UniWebViewDialog dialog, Activity activity, MotionEvent event) {
      boolean touchHandledByAnotherDialog = false;
      Iterator var5 = this.allDialogs().iterator();

      while(true) {
         UniWebViewDialog d;
         do {
            if (!var5.hasNext()) {
               if (!touchHandledByAnotherDialog) {
                  activity.dispatchTouchEvent(event);
               }

               return false;
            }

            d = (UniWebViewDialog)var5.next();
         } while(d == dialog);

         d.getWebView().requestFocus();
         d.touchFromAnotherDialog = true;
         touchHandledByAnotherDialog = d.dispatchTouchEvent(event) || touchHandledByAnotherDialog;
         d.touchFromAnotherDialog = false;
      }
   }

   ArrayList<UniWebViewDialog> getShowingDialogs() {
      return this.showingDialogs;
   }
}
