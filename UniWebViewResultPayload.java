package com.onevcat.uniwebview;

import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

public class UniWebViewResultPayload {
   String identifier;
   String resultCode;
   String data;

   public UniWebViewResultPayload(String jsonString) {
      try {
         JSONObject obj = new JSONObject(jsonString);
         this.identifier = obj.getString("identifier");
         this.resultCode = obj.getString("resultCode");
         this.data = obj.getString("data");
      } catch (JSONException var3) {
         var3.printStackTrace();
      }

   }

   public UniWebViewResultPayload(String identifier, String resultCode, String data) {
      this.identifier = identifier;
      this.resultCode = resultCode;
      this.data = data;
   }

   public String toString() {
      HashMap<String, String> dic = new HashMap();
      dic.put("identifier", this.identifier);
      dic.put("resultCode", this.resultCode);
      dic.put("data", this.data);
      JSONObject obj = new JSONObject(dic);
      return obj.toString();
   }
}
