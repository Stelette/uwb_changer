package com.onevcat.uniwebview;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebChromeClient;
import android.webkit.WebChromeClient.CustomViewCallback;
import android.widget.FrameLayout;
import android.widget.VideoView;

public class VideoEnabledWebChromeClient extends WebChromeClient implements OnPreparedListener, OnCompletionListener, OnErrorListener {
   private View activityNonVideoView;
   private ViewGroup activityVideoView;
   private View loadingView;
   private VideoEnabledWebView webView;
   private boolean isVideoFullscreen;
   private FrameLayout videoViewContainer;
   private CustomViewCallback videoViewCallback;
   private VideoEnabledWebChromeClient.ToggledFullscreenCallback toggledFullscreenCallback;

   public VideoEnabledWebChromeClient() {
   }

   public VideoEnabledWebChromeClient(View activityNonVideoView, ViewGroup activityVideoView) {
      this.activityNonVideoView = activityNonVideoView;
      this.activityVideoView = activityVideoView;
      this.loadingView = null;
      this.webView = null;
      this.isVideoFullscreen = false;
   }

   public VideoEnabledWebChromeClient(View activityNonVideoView, ViewGroup activityVideoView, View loadingView) {
      this.activityNonVideoView = activityNonVideoView;
      this.activityVideoView = activityVideoView;
      this.loadingView = loadingView;
      this.webView = null;
      this.isVideoFullscreen = false;
   }

   public VideoEnabledWebChromeClient(View activityNonVideoView, ViewGroup activityVideoView, View loadingView, VideoEnabledWebView webView) {
      this.activityNonVideoView = activityNonVideoView;
      this.activityVideoView = activityVideoView;
      this.loadingView = loadingView;
      this.webView = webView;
      this.isVideoFullscreen = false;
   }

   public boolean isVideoFullscreen() {
      return this.isVideoFullscreen;
   }

   public void setOnToggledFullscreen(VideoEnabledWebChromeClient.ToggledFullscreenCallback callback) {
      this.toggledFullscreenCallback = callback;
   }

   public void onShowCustomView(View view, CustomViewCallback callback) {
      Logger.getInstance().verbose("onShowCustomView!!!");
      if (view instanceof FrameLayout) {
         FrameLayout frameLayout = (FrameLayout)view;
         View focusedChild = frameLayout.getFocusedChild();
         this.isVideoFullscreen = true;
         this.videoViewContainer = frameLayout;
         this.videoViewCallback = callback;
         this.activityNonVideoView.setVisibility(4);
         this.activityVideoView.addView(this.videoViewContainer, new LayoutParams(-1, -1));
         this.activityVideoView.setVisibility(0);
         if (focusedChild instanceof VideoView) {
            VideoView videoView = (VideoView)focusedChild;
            videoView.setOnPreparedListener(this);
            videoView.setOnCompletionListener(this);
            videoView.setOnErrorListener(this);
         } else if (this.webView != null && this.webView.getSettings().getJavaScriptEnabled() && focusedChild instanceof SurfaceView) {
            String js = "javascript:";
            js = js + "var _ytrp_html5_video_last;";
            js = js + "var _ytrp_html5_video = document.getElementsByTagName('video')[0];";
            js = js + "if (_ytrp_html5_video != undefined && _ytrp_html5_video != _ytrp_html5_video_last) {";
            js = js + "_ytrp_html5_video_last = _ytrp_html5_video;";
            js = js + "function _ytrp_html5_video_ended() {";
            js = js + "window.location.href = 'uniwebviewinternal://__uniwebview_internal_video_end';";
            js = js + "}";
            js = js + "_ytrp_html5_video.addEventListener('ended', _ytrp_html5_video_ended);";
            js = js + "}";
            this.webView.loadUrl(js);
         }

         if (this.toggledFullscreenCallback != null) {
            this.toggledFullscreenCallback.toggledFullscreen(true);
         }
      }

   }

   public void onShowCustomView(View view, int requestedOrientation, CustomViewCallback callback) {
      this.onShowCustomView(view, callback);
   }

   public void onHideCustomView() {
      if (this.isVideoFullscreen) {
         this.activityVideoView.setVisibility(4);
         this.activityVideoView.removeView(this.videoViewContainer);
         this.activityNonVideoView.setVisibility(0);
         if (this.videoViewCallback != null && !this.videoViewCallback.getClass().getName().contains(".chromium.")) {
            this.videoViewCallback.onCustomViewHidden();
         }

         this.isVideoFullscreen = false;
         this.videoViewContainer = null;
         this.videoViewCallback = null;
         if (this.toggledFullscreenCallback != null) {
            this.toggledFullscreenCallback.toggledFullscreen(false);
         }
      }

      Logger.getInstance().verbose("Clear Focus");
      this.webView.clearFocus();
   }

   public View getVideoLoadingProgressView() {
      if (this.loadingView != null) {
         this.loadingView.setVisibility(0);
         return this.loadingView;
      } else {
         return super.getVideoLoadingProgressView();
      }
   }

   public void onPrepared(MediaPlayer mp) {
      if (this.loadingView != null) {
         this.loadingView.setVisibility(8);
      }

   }

   public void onCompletion(MediaPlayer mp) {
      this.onHideCustomView();
   }

   public boolean onError(MediaPlayer mp, int what, int extra) {
      return false;
   }

   public boolean onBackPressed() {
      if (this.isVideoFullscreen) {
         this.onHideCustomView();
         return true;
      } else {
         return false;
      }
   }

   public interface ToggledFullscreenCallback {
      void toggledFullscreen(boolean var1);
   }
}
