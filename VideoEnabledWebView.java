package com.onevcat.uniwebview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

public class VideoEnabledWebView extends WebView {
   private VideoEnabledWebChromeClient videoEnabledWebChromeClient;

   public void notifyVideoEnd() {
      (new Handler(Looper.getMainLooper())).post(new Runnable() {
         public void run() {
            if (VideoEnabledWebView.this.videoEnabledWebChromeClient != null) {
               VideoEnabledWebView.this.videoEnabledWebChromeClient.onHideCustomView();
            }

         }
      });
   }

   public VideoEnabledWebView(Context context) {
      super(context);
   }

   public VideoEnabledWebView(Context context, AttributeSet attrs) {
      super(context, attrs);
   }

   public VideoEnabledWebView(Context context, AttributeSet attrs, int defStyle) {
      super(context, attrs, defStyle);
   }

   public boolean isVideoFullscreen() {
      return this.videoEnabledWebChromeClient != null && this.videoEnabledWebChromeClient.isVideoFullscreen();
   }

   @SuppressLint({"SetJavaScriptEnabled"})
   public void setWebChromeClient(WebChromeClient client) {
      this.getSettings().setJavaScriptEnabled(true);
      if (client instanceof VideoEnabledWebChromeClient) {
         this.videoEnabledWebChromeClient = (VideoEnabledWebChromeClient)client;
      }

      super.setWebChromeClient(client);
   }
}
